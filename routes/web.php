<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/uikit', function () {
    return view('uikit');
});

Route::get('/', function () {
    return view('main');
});

Route::get('/service', function () {
    return view('services');
});

Route::get('/service/1', function () {
    return view('service-group');
});

Route::get('/service/1/2', function () {
    return view('service');
});

Route::get('/price', function () {
    return view('price');
});


Route::get('/products', function () {
    return view('products');
});


Route::get('/products/1', function () {
    return view('product-group');
});

Route::get('/products/1/2', function () {
    return view('product');
});


Route::get('/industries', function () {
    return view('industries');
});


Route::get('/industries/1', function () {
    return view('industry-group');
});


Route::get('/industries/1/2', function () {
    return view('industry');
});

Route::get('/about', function () {
    return view('about');
});


Route::get('/projects', function () {
    return view('projects');
});

Route::get('/projects/1', function () {
    return view('project');
});


Route::get('/blog', function () {
    return view('blog');
});

Route::get('/blog/1', function () {
    return view('article');
});

Route::get('/offers', function () {
    return view('offers');
});

Route::get('/offers/1', function () {
    return view('offer');
});

Route::get('/reviews', function () {
    return view('reviews');
});

Route::get('/contacts', function () {
    return view('contacts');
});