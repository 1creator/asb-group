<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UI Kit</title>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <link rel="stylesheet" type="text/css" href="/css/uikit.css">
    <script src="/js/app.js"></script>
</head>
<body class="uikit">
<section class="uikit-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6 mb-5 mb-lg-0">
                <h4 class="uikit-section__header mb-5">01. Модульная сетка</h4>
                <div class="row mb-3" style="height: 200px">
                    <div class="col">
                        <div class="bg-additional h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-additional h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-additional h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-additional h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-additional h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-additional h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-additional h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-additional h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-additional h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-additional h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-additional h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-additional h-100"></div>
                    </div>
                </div>
                <div class="small text-center">
                    Grid 12 - Width 1140px / Offset 36px Gutter width - 30px Column width - 65 px
                </div>
            </div>
            <div class="col-12 col-lg-6 colors">
                <h4 class="uikit-section__header mb-5">02. Цвета</h4>
                <div class="row">
                    <div class="col">
                        <div class="colors-item">
                            <div class="colors-item__circle bg-primary"></div>
                            <div class="colors-item__name">Main</div>
                            <div class="colors-item__code text-additional">5023CE</div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="colors-item">
                            <div class="colors-item__circle bg-secondary"></div>
                            <div class="colors-item__name">Secondary</div>
                            <div class="colors-item__code text-additional">3172FD</div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="colors-item">
                            <div class="colors-item__circle bg-additional"></div>
                            <div class="colors-item__name">+</div>
                            <div class="colors-item__code text-additional">B8D9FF</div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="colors-item">
                            <div class="colors-item__circle bg-info"></div>
                            <div class="colors-item__name">Info</div>
                            <div class="colors-item__code text-additional">#3023AE</div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="colors-item">
                            <div class="colors-item__circle bg-black"></div>
                            <div class="colors-item__name">Текст</div>
                            <div class="colors-item__code text-additional">#000000</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="uikit-section typography">
    <div class="container">
        <h4 class="uikit-section__header mb-5">03. Типографика</h4>
        <div class="typography__items">
            <h1>Супер заголовок/Roboto/56px/Medium</h1>
            <h2>Обычный заголовок/Roboto/48px/Medium</h2>
            <h3>Средний заголовок/Roboto/24px/Medium</h3>
            <h4>Мини заголовок/Roboto/18px/Regular</h4>
            <div>Основной текст/Roboto/16px/Regular</div>
            <div class="small">Маленький текст/Roboto/14px/Regular</div>
            <div class="btn">Текст кнопок/Roboto/16px/Regular</div>
            <div class="nav">Mеню текст/14px/18px/Medium</div>
        </div>
    </div>
</section>
<section class="uikit-section typography-sample">
    <div class="container">
        <h4 class="uikit-section__header mb-5">04. Типографика</h4>
        <h3 class="text-secondary">Для чего используют «1С:ERP»?</h3>
        <div class="row mb-5">
            <div class="col-12 col-sm-6">
                1С:ERP — логическое продолжение развития программного продукта «1С:Управление производственным
                предприятием
                8». 1С:ERP имеет не только гораздо более широкий функционал, но и явные технологические преимущества.
                Учитывая то, что модернизация и внедрение системы класса ERP на предприятии – сложная задача, переход с
                1С:УПП на ERP может быть осуществлен только с помощью команды специалистов
            </div>
        </div>
        <table class="table table-borderless table-responsive mb-5">
            <thead>
            <tr class="text-secondary">
                <th>Услуга</th>
                <th>Время</th>
                <th>Цена/час</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="pr-lg-5">
                    <h3>Внедрение программ 1С</h3>
                    <div>
                        Комплекс работ по запуску
                        и адаптации программ 1С под нужды
                        и потребности клиента.
                    </div>
                </td>
                <td>
                    <h3 class="text-nowrap">от&nbsp;1&nbsp;ч.</h3>
                </td>
                <td>
                    <h3 class="text-nowrap">от&nbsp;1&nbsp;000&nbsp;р.</h3>
                </td>
                <td>
                    <button class="btn btn-secondary w-100">Выбрать</button>
                </td>
            </tr>
            <tr>
                <td class="pr-lg-5">
                    <h3>Обновление программ 1С</h3>
                    <div>
                        Обновление платформы, конфигурации
                        и классификаторов программных продуктов.
                    </div>
                </td>
                <td>
                    <h3 class="text-nowrap">от&nbsp;1&nbsp;ч.</h3>
                </td>
                <td>
                    <h3 class="text-nowrap">от&nbsp;1&nbsp;000&nbsp;р.</h3>
                </td>
                <td>
                    <button class="btn btn-secondary w-100">Выбрать</button>
                </td>
            </tr>
            <tr>
                <td class="pr-lg-5">
                    <h3>Консультация по работе с программой</h3>
                    <div>
                        Архивация, перепроведение, граница последовательности,
                        периоды, методика работы с программой.
                    </div>
                </td>
                <td>
                    <h3 class="text-nowrap">от&nbsp;1&nbsp;ч.</h3>
                </td>
                <td>
                    <h3 class="text-nowrap">от&nbsp;1&nbsp;000&nbsp;р.</h3>
                </td>
                <td>
                    <button class="btn btn-secondary w-100">Выбрать</button>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="row">
            <div class="col">
                <h3 class="text-secondary">Для чего используют «1С:ERP»?</h3>
                <ul>
                    <li>Своевременное обновление конфигураций</li>
                    <li>Своевременное обновление конфигураций, дает</li>
                    <li>Своевременное обновление конфигураций, дает на получение обновление конфигураций</li>
                    <li>Своевременное обновление конфигураций, дает</li>
                    <li>Своевременное обновление конфигураций</li>
                </ul>
            </div>
            <a name="nav"></a>
            <div class="col offset-1">
                <nav class="navbar navbar-expand-lg">
                    <ul class="list-unstyled navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#nav">Услуги</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#nav">Программы 1С</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#nav">Отраслевые решения</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#nav">Проекты</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</section>
<section class="uikit-section interface">
    <div class="container">
        <h4 class="uikit-section__header mb-5">05. Элементы интерфейса</h4>
        <div class="row mb-5">
            <div class="col-12 col-lg-6 mb-3">
                <div class="row mb-3 p-1">
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-primary w-100">Заказать звонок</button>
                    </div>
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-primary w-100">Заказать звонок</button>
                    </div>
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-primary w-100">Заказать звонок</button>
                    </div>
                </div>
                <div class="row mb-3 p-1">
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-secondary w-100">Акции</button>
                    </div>
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-secondary w-100">Акции</button>
                    </div>
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-secondary w-100">Акции</button>
                    </div>
                </div>
                <div class="row bg-secondary p-1">
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-light w-100">Акции</button>
                    </div>
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-light w-100">Акции</button>
                    </div>
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-light w-100">Акции</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="row">
                    <div class="col-12 mb-3">
                        <div class="nav nav-tabs btn-group btn-group-tabs w-100" role="tablist">
                            <a class="btn btn-tab active" data-toggle="tab" href="#programs" data-role="tab"
                               aria-controls="programs">
                                ПРОГРАММЫ
                            </a>
                            <a class="btn btn-tab" data-toggle="tab" href="#services" data-role="tab"
                               aria-controls="services">
                                УСЛУГИ
                            </a>
                            <a class="btn btn-tab" data-toggle="tab" href="#industries" data-role="tab"
                               aria-controls="industries">
                                ОТРАСЛИ
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-12">
                <button class='btn btn-link'>
                    Читать больше
                    <i class="text-primary icon-sm icon-caret-right"></i>
                </button>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-12 col-lg-6 mb-3">
                <div class="form-row">
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Имя">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Номер телефона">
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="row">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <div class="custom-control custom-radio mb-3">
                                    <input type="radio" id="radio1" name="radio" class="custom-control-input">
                                    <label class="custom-control-label" for="radio1">Покой</label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="custom-control custom-radio mb-3">
                                    <input type="radio" id="radio2" name="radio" checked class="custom-control-input">
                                    <label class="custom-control-label" for="radio2">Активное поле ввода</label>
                                </div>
                            </div>
                            <div class="col-6 mb-3"><i class="text-primary icon-arrow-up"></i></div>
                            <div class="col-6 mb-3"><i class="text-primary icon-arrow-right"></i></div>
                            <div class="col-6 mb-3"><i class="text-primary icon-arrow-down"></i></div>
                            <div class="col-6 mb-3"><i class="text-primary icon-arrow-left"></i></div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-6 mb-3">
                                <div class="bg-secondary p-3 text-center">
                                    <button class="btn btn-secondary btn-circle"><i class="icon-close"></i></button>
                                </div>
                            </div>
                            <div class="col-6 mb-3">
                                <div class="p-3 text-center">
                                    <button class="btn btn-secondary btn-circle"><i class="icon-close"></i></button>
                                </div>
                            </div>
                            <div class="col-4 mb-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" checked class="custom-control-input" id="check1">
                                    <label class="custom-control-label" for="check1"></label>
                                </div>
                            </div>
                            <div class="col-4 mb-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="check2">
                                    <label class="custom-control-label" for="check2"></label>
                                </div>
                            </div>
                            <div class="col-4 mb-3">
                                <i class="text-primary icon-search"></i>
                            </div>
                            <div class="col-6 mb-3">
                                <i class="text-primary icon-arrow"></i>
                            </div>
                            <div class="col-6 mb-3">
                                <i class="text-primary icon-arrow"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-12 col-lg-4 mb-3">
                <div class="doughnut">
                    <canvas class="chart doughnut__chart chart--doughnut"></canvas>
                    <div class="doughnut__text font-weight-bold">252 000 пользуются услугами</div>
                </div>
            </div>
            <div class="col-12 col-lg-4 offset-lg-4">
                <div class="row mb-5">
                    <div class="col-4">
                        <button class="btn btn-sm btn-primary btn-circle mr-1">
                            <i class="icon-triangle-left"></i>
                        </button>
                        <button class="btn btn-sm btn-primary btn-circle">
                            <i class="icon-triangle-right"></i>
                        </button>
                    </div>
                    <div class="col-4">
                        <button class="btn btn-sm btn-primary btn-circle mr-1">
                            <i class="icon-triangle-left"></i>
                        </button>
                        <button class="btn btn-sm btn-primary btn-circle">
                            <i class="icon-triangle-right"></i>
                        </button>
                    </div>
                    <div class="col-4">
                        <button class="btn btn-sm btn-primary btn-circle mr-1">
                            <i class="icon-triangle-left"></i>
                        </button>
                        <button class="btn btn-sm btn-primary btn-circle">
                            <i class="icon-triangle-right"></i>
                        </button>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="dropdown-menu" style="position: static; display: block">
                        <a class="dropdown-item">Поликлиники, Стационары, Скорая</a>
                        <a class="dropdown-item active">Санатории</a>
                        <a class="dropdown-item">Управление здравоохранением</a>
                        <a class="dropdown-item">Фармацевтика</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <input class="w-100" type="range" min="0" max="100">
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-12 col-lg-4">

                <div class="card py-40 card-bg-additional text-center">
                    <div class="card-body">
                        <div class="mb-20">
                            <img src="/images/icons/people-money.svg">
                        </div>
                        <h5 class="card-title" style="height:80px">1С:ERP<br/>Управление предприятием</h5>
                        <p class="card-text mb-40">1С:ERP Управление предприятием 1С:ERP Управление
                            предприятием</p>
                        <button class="btn btn-secondary">Читать подробнее</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card py-40 card-bg-additional text-center">
                    <div class="card-body">
                        <div class="mb-20">
                            <img src="/images/icons/people-money.svg">
                        </div>
                        <h5 class="card-title" style="height:80px">1С:ERP<br/>Управление предприятием</h5>
                        <p class="card-text mb-40">1С:ERP Управление предприятием 1С:ERP Управление
                            предприятием</p>
                        <button class="btn btn-secondary">Читать подробнее</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">

                <div class="card py-40 card-bg-additional text-center">
                    <div class="card-body">
                        <div class="mb-20">
                            <img src="/images/icons/people-money.svg">
                        </div>
                        <h5 class="card-title" style="height:80px">1С:ERP<br/>Управление предприятием</h5>
                        <p class="card-text mb-40">1С:ERP Управление предприятием 1С:ERP Управление
                            предприятием</p>
                        <button class="btn btn-secondary">Читать подробнее</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col">
                <div class="card card-horizontal text-center">
                    <div class="card-body">
                        <h5 class="card-title">Переход с 1С:УПП на 1C:ERP </h5>
                        <p class="card-text">
                            Индивидуальная доработка<br/>
                            Внешняя специфика<br/>
                            Обучение сотрудников<br/></p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card card-horizontal text-center">
                    <div class="card-body">
                        <h5 class="card-title">1С:ERP</h5>
                        <p class="card-text">
                            Индивидуальная доработка<br/>
                            Внешняя специфика<br/>
                            Обучение сотрудников<br/></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="card card-shadow text-center ">
                    <div class="card-body">
                        <p class="card-text">Карточка с тенью</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>
