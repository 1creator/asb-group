<section class="best-offers-wrap">
    <div class="container py-80 best-offers">
        <div class="row text-white mb-80">
            <div class="col-12 col-md-4 best-offers__text">
                <h2 class="mb-4">Наши лучшие предложения</h2>
                <div>
                    Возможности, которые вы получаете:<br/>
                    Использование широкого функционала 1С
                </div>
            </div>
            <div class="col-md-8 best-offers__image-wrap">
                <img src="/images/computer.svg">
            </div>
        </div>
        <div class="glide" data-glide-gap="30">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides mb-80">
                    <li class="glide__slide">
                        <div class="card py-20 card-bg-additional text-center">
                            <div class="card-body">
                                <div class="mb-20">
                                    <img src="/images/icons/people-money.svg">
                                </div>
                                <h5 class="card-title" style="height:80px">1С:ERP<br/>Управление предприятием</h5>
                                <p class="card-text mb-40">1С:ERP Управление предприятием 1С:ERP Управление
                                    предприятием</p>
                                <button class="btn btn-secondary">Читать подробнее</button>
                            </div>
                        </div>
                    </li>
                    <li class="glide__slide">
                        <div class="card py-20 card-bg-additional text-center">
                            <div class="card-body">
                                <div class="mb-20">
                                    <img src="/images/icons/people-money.svg">
                                </div>
                                <h5 class="card-title" style="height:80px">ИТС</h5>
                                <p class="card-text mb-40">1С:ERP Управление предприятием 1С:ERP Управление
                                    предприятием</p>
                                <button class="btn btn-secondary">Читать подробнее</button>
                            </div>
                        </div>
                    </li>
                    <li class="glide__slide">
                        <div class="card py-20 card-bg-additional text-center">
                            <div class="card-body">
                                <div class="mb-20">
                                    <img src="/images/icons/people-money.svg">
                                </div>
                                <h5 class="card-title" style="height:80px">1С: Обучение</h5>
                                <p class="card-text mb-40">1С:ERP Управление предприятием 1С:ERP Управление
                                    предприятием</p>
                                <button class="btn btn-secondary">Читать подробнее</button>
                            </div>
                        </div>
                    </li>
                </ul>
                <div data-glide-el="controls" class="text-center">
                    <button data-glide-dir="<" class="btn btn-sm border-white btn-secondary btn-circle mr-2">
                        <i class="icon-triangle-left"></i>
                    </button>
                    <button data-glide-dir=">" class="btn btn-sm border-white btn-secondary btn-circle">
                        <i class="icon-triangle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>