<section>
    <div class="container">
        <div class="row text-center">
            <div class="col-12 col-md-10 offset-md-1 mb-80">
                <h1 class="mb-20">ASB GROUP</h1>
                <h3 class="mb-20">Команда опытных специалистов в области 1С установки и внедрения</h3>
                <small class="mx-md-20">Многолетний опыт работы с бизнесом самого разного масштаба — от
                    индивидуальных<br class="d-none d-lg-inline"/>
                    предпринимателей до больших предприятий с разветвлённой структурой.
                </small>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4 service-item mb-md-0">
                <div class="service-item__img-wrap">
                    <img class="mb-3" src="/images/responsibility.svg">
                </div>
                <h3>Мы ответственны</h3>
                <div class="text-gray">Использование широкого функционала 1С</div>
            </div>
            <div class="col-12 col-md-4 service-item mb-md-0">
                <div class="service-item__img-wrap">
                    <img class="mb-3" src="/images/fast-work.svg">
                </div>
                <h3>Мы быстро работаем</h3>
                <div class="text-gray">
                    Доработка функцианала<br/>
                    Обучение персонала
                </div>
            </div>
            <div class="col-12 col-md-4 service-item mb-md-0">
                <div class="service-item__img-wrap">
                    <img class="mb-3" src="/images/experience.svg">
                </div>
                <h3>10 лет опыт</h3>
                <div class="text-gray">
                    Получить индивидуально настроенную систему под&nbsp;текущие&nbsp;бизнес-процессы предприятия
                </div>
            </div>
        </div>
    </div>
</section>