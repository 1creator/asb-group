<section>
    <div class="container">
        <div class="row mb-80">
            <div class="col text-center">
                <h1>Что о нас говорят</h1>
                <a class="h4 text-secondary" href="/reviews">Читать все отзывы</a>
            </div>
        </div>
        <div class="glide mb-80" data-glide-perview="2" data-glide-gap="40">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides mb-40" style="overflow: visible">
                    <li class="glide__slide">
                        <div class="card card-shadow review-card">
                            <div class="card-header review-card__video-wrap">
                                <button data-glide-dir=">" class="btn btn-sm btn-light btn-circle">
                                    <i class="icon-triangle-right"></i>
                                </button>
                            </div>
                            <div class="card-body">
                                <div class="d-flex align-items- mb-4">
                                    <div class="review-card__avatar mr-3">
                                        <img src="/images/avatar.png">
                                    </div>
                                    <div>
                                        <h3>Вадим</h3>
                                        <div class="text-gray">Компания 1С</div>
                                    </div>
                                </div>
                                <div class="review-card__text">
                                    Многолетний опыт работы с бизнесом самого разного масштаба — от индивидуальных
                                    предпринимателей до больших предприятий с разветвлённой структурой.
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <div data-glide-el="controls" class="text-center">
                    <button data-glide-dir="<" class="btn btn-sm btn-primary btn-circle mr-2">
                        <i class="icon-triangle-left"></i>
                    </button>
                    <button data-glide-dir=">" class="btn btn-sm btn-primary btn-circle">
                        <i class="icon-triangle-right"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="glide" data-glide-perview="2" data-glide-gap="40">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides mb-40" style="overflow: visible">
                    <li class="glide__slide">
                        <div class="card card-shadow review-card mb-40">
                            <div class="card-body">
                                <div class="d-flex align-items- mb-4">
                                    <div class="review-card__avatar mr-3">
                                        <img src="/images/avatar.png">
                                    </div>
                                    <div>
                                        <h3>Вадим</h3>
                                        <div class="text-gray">Компания 1С</div>
                                    </div>
                                </div>
                                <div class="review-card__text">
                                    Многолетний опыт работы с бизнесом самого разного масштаба — от индивидуальных
                                    предпринимателей до больших предприятий с разветвлённой структурой.
                                </div>
                                <button class="btn btn-link text-decoration-none mt-2 mb-2"
                                        data-toggle="collapse" data-target="#review-card-1-read-more">
                                    Читать больше
                                    <i class="text-primary icon-sm icon-arrow-down"></i>
                                </button>
                                <div class="collapse" id="review-card-1-read-more">
                                    <div class="d-flex mb-2">
                                        ................<br/>
                                        ................<br/>
                                        ................<br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-shadow review-card">
                            <div class="card-body">
                                <div class="d-flex align-items- mb-4">
                                    <div class="review-card__avatar mr-3">
                                        <img src="/images/avatar.png">
                                    </div>
                                    <div>
                                        <h3>Вадим</h3>
                                        <div class="text-gray">Компания 1С</div>
                                    </div>
                                </div>
                                <div class="review-card__text">
                                    Многолетний опыт работы с бизнесом самого разного масштаба — от индивидуальных
                                    предпринимателей до больших предприятий с разветвлённой структурой.
                                </div>
                                <button class="btn btn-link text-decoration-none mt-2 mb-2"
                                        data-toggle="collapse" data-target="#review-card-2-read-more">
                                    Читать больше
                                    <i class="text-primary icon-sm icon-arrow-down"></i>
                                </button>
                                <div class="collapse" id="review-card-2-read-more">
                                    <div class="d-flex mb-2">
                                        ................<br/>
                                        ................<br/>
                                        ................<br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="glide__slide">
                        <div class="card card-shadow review-card mb-40">
                            <div class="card-body">
                                <div class="d-flex align-items- mb-4">
                                    <div class="review-card__avatar mr-3">
                                        <img src="/images/avatar.png">
                                    </div>
                                    <div>
                                        <h3>Вадим</h3>
                                        <div class="text-gray">Компания 1С</div>
                                    </div>
                                </div>
                                <div class="review-card__text">
                                    Многолетний опыт работы с бизнесом самого разного масштаба — от индивидуальных
                                    предпринимателей до больших предприятий с разветвлённой структурой.
                                </div>
                                <button class="btn btn-link text-decoration-none mt-2 mb-2"
                                        data-toggle="collapse" data-target="#review-card-3-read-more">
                                    Читать больше
                                    <i class="text-primary icon-sm icon-arrow-down"></i>
                                </button>
                                <div class="collapse" id="review-card-3-read-more">
                                    <div class="d-flex mb-2">
                                        ................<br/>
                                        ................<br/>
                                        ................<br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-shadow review-card">
                            <div class="card-body">
                                <div class="d-flex align-items- mb-4">
                                    <div class="review-card__avatar mr-3">
                                        <img src="/images/avatar.png">
                                    </div>
                                    <div>
                                        <h3>Вадим</h3>
                                        <div class="text-gray">Компания 1С</div>
                                    </div>
                                </div>
                                <div class="review-card__text">
                                    Многолетний опыт работы с бизнесом самого разного масштаба — от индивидуальных
                                    предпринимателей до больших предприятий с разветвлённой структурой.
                                </div>
                                <button class="btn btn-link text-decoration-none mt-2 mb-2"
                                        data-toggle="collapse" data-target="#review-card-4-read-more">
                                    Читать больше
                                    <i class="text-primary icon-sm icon-arrow-down"></i>
                                </button>
                                <div class="collapse" id="review-card-4-read-more">
                                    <div class="d-flex mb-2">
                                        ................<br/>
                                        ................<br/>
                                        ................<br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <div data-glide-el="controls" class="text-center">
                    <button data-glide-dir="<" class="btn btn-sm btn-primary btn-circle mr-2">
                        <i class="icon-triangle-left"></i>
                    </button>
                    <button data-glide-dir=">" class="btn btn-sm btn-primary btn-circle">
                        <i class="icon-triangle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>