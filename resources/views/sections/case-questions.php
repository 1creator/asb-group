<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 offset-sm-0 offset-md-1 align-self-end">
                <div class="row">
                    <div class="col-12 questions-item mb-40">
                        <h1 class="text-secondary questions-item__index">01</h1>
                        <h3 class="questions-item__question">Не удобно работать в 1С?</h3>
                        <div class="text-muted questions-item__text pr-md-5">
                            Использование широкого функционала 1С
                        </div>
                    </div>
                    <div class="col-12 questions-item mb-40">
                        <h1 class="text-secondary questions-item__index">02</h1>
                        <h3 class="questions-item__question">Неквалифицированные сотрудники?</h3>
                        <div class="text-muted questions-item__text">
                            Доработка функционала<br/>
                            Обучение персонала
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 offset-md-1 mb-5 align-self-center">
                <img src="/images/funny-man.svg">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-6 col-md-4 offset-md-1">
                <div class="questions-item mb-40">
                    <h1 class="text-secondary questions-item__index">03</h1>
                    <h3 class="questions-item__question">Не удобно управлять?</h3>
                    <div class="text-muted questions-item__text pr-md-80">
                        Получить индивидуально настроенную систему под&nbsp;текущие бизнес-процессы предприятия
                    </div>
                </div>
                <div class="questions-item mb-40 mb-md-0">
                    <h1 class="text-secondary questions-item__index">04</h1>
                    <h3 class="questions-item__question">Не получается установить?</h3>
                    <div class="text-muted questions-item__text">
                        Экономии времени на информации<br/>
                        Оптимизации управления<br/>
                        Хранения корпоративных документов
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 offset-md-2 d-flex flex-column justify-content-between">
                <div class="questions-item mb-40">
                    <h1 class="text-secondary questions-item__index">05</h1>
                    <h3 class="questions-item__question">Слишком дорого?</h3>
                    <div class="text-muted questions-item__text">
                        Экономия до 50 тысяч рублей в месяц
                    </div>
                </div>
                <div class="questions-item">
                    <h1 class="text-secondary questions-item__index">06</h1>
                    <h3 class="questions-item__question">Не подходит типовое решение?</h3>
                    <div class="text-muted questions-item__text">
                        Индивидуальная доработка<br/>
                        Внешняя специфика
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>