<section class="why-we">
    <div class="container text-black">
        <div class="text-center">
            <h2 class="mb-20">Почему выбирают нас?</h2>
            <div class="mb-5">ASB GROUP - Официальный партнёр компании «1С»</div>
        </div>
        <img src="/images/map.svg" class="d-block d-md-none mb-4">
        <div class="row">
            <div class="col-12 col-md-3 no-gutters">
                <div class="row h-100 align-items-end">
                    <div class="mb-3 col-sm-6 col-md-12">
                        <h1>8 лет</h1>
                        <div>успешное существование на рынке</div>
                    </div>
                    <div class="mb-4 col-sm-6 col-md-12">
                        <h1>20</h1>
                        <div>высококвалифицированных сотрудников</div>
                    </div>
                    <div class="mb-4 col-sm-6 col-md-12">
                        <h1>10 000+</h1>
                        <div style="letter-spacing: -0.3px;">численность персонала компаний, которые мы автоматизируем</div>
                    </div>
                    <div class="md-4 mb-4 mb-md-0 col-sm-6 col-md-12">
                        <h1>100+</h1>
                        <div>средних и крупных проектов</div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 offset-md-1 align-self-end">
                <div class="d-none d-md-block">
                    <img src="/images/map.svg">
                </div>
                <div class="row align-items-end">
                    <div class="col-12 col-sm-6 mb-6 mb-md-0 d-flex justify-content-md-end">
                        <div>
                            <h1>8 – 10 лет</h1>
                            <div>стаж работы наших сотрудников</div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 text-right mb-6 mb-md-0">
                        <button class='btn btn-link'>
                            Читать больше
                            <i class="text-primary icon-sm icon-caret-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>