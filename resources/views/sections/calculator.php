<section class="-calculator">
    <div class="container">
        <div class="row mb-40">
            <div class="col text-center">
                <h1>Рассчет стоимости сопровождения</h1>
                <div class="row">
                    <div class="col-12 col-md-6 text-md-right">
                        Получите рассчет прямо сейчас!
                    </div>
                    <div class="col-12 col-md-6 text-md-left">
                        <button class="ml-3 btn btn-link text-secondary">
                            Cмотреть прайс
                            <i class="text-secondary icon-sm icon-caret-down"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 mb-5 mb-md-0">
                <div class="mb-5">
                    <div class="mb-3">
                        Количество пользователей 1С
                    </div>
                    <input class="w-100" type="range" min="0" max="100">
                </div>
                <div class="mb-5">
                    <div class="mb-3">
                        Количество пользователей 1С
                    </div>
                    <input class="w-100" type="range" min="0" max="100">
                </div>
                <div class="mb-5">
                    <div class="mb-3">
                        Использование конфигураций 1С
                    </div>
                    <div class="d-flex mb-2">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="radio1" name="radio" class="custom-control-input">
                            <label class="custom-control-label" for="radio1">Установка 1С</label>
                        </div>
                        <div class="custom-control custom-radio ml-5">
                            <input type="radio" id="radio2" name="radio" class="custom-control-input">
                            <label class="custom-control-label" for="radio2">Установка 1С</label>
                        </div>
                    </div>
                    <div class="d-flex mb-2">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="radio3" name="radio1" class="custom-control-input">
                            <label class="custom-control-label" for="radio3">Установка 1С</label>
                        </div>
                        <div class="custom-control custom-radio ml-5">
                            <input type="radio" id="radio4" name="radio1" class="custom-control-input">
                            <label class="custom-control-label" for="radio4">Установка 1С</label>
                        </div>
                    </div>
                    <div class="d-flex mb-2">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="radio5" name="radio2" class="custom-control-input">
                            <label class="custom-control-label" for="radio5">Установка 1С</label>
                        </div>
                        <div class="custom-control custom-radio ml-5">
                            <input type="radio" id="radio6" name="radio2" class="custom-control-input">
                            <label class="custom-control-label" for="radio6">Установка 1С</label>
                        </div>
                    </div>
                    <div class="d-flex mb-2">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="radio7" name="radio3" class="custom-control-input">
                            <label class="custom-control-label" for="radio7">Установка 1С</label>
                        </div>
                        <div class="custom-control custom-radio ml-5">
                            <input type="radio" id="radio8" name="radio3" class="custom-control-input">
                            <label class="custom-control-label" for="radio8">Установка 1С</label>
                        </div>
                    </div>
                </div>
                <div class="mb-5">
                    <div class="mb-3">
                        Количество часов работы программиста
                    </div>
                    <input class="w-100" type="range" min="0" max="100">
                </div>
                <div>
                    <button class="btn btn-link text-secondary text-decoration-none mb-2"
                            data-toggle="collapse" data-target="#additionalService">
                        Дополнительные услуги
                        <i class="text-secondary icon-sm icon-arrow-down"></i>
                    </button>
                    <div class="collapse" id="additionalService">
                        <div class="d-flex mb-2">
                            <div class="custom-control custom-radio">
                                <input type="radio" id="radio9" name="radio4" class="custom-control-input">
                                <label class="custom-control-label" for="radio9">Установка 1С</label>
                            </div>
                            <div class="custom-control custom-radio ml-5">
                                <input type="radio" id="radio10" name="radio4" class="custom-control-input">
                                <label class="custom-control-label" for="radio10">Установка 1С</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 pl-md-80">
                <div class="card card-shadow p-40">
                    <div class="mb-5">
                        <h3 class="mb-3 text-black">СТОИМОСТЬ СОПРОВОЖДЕНИЯ</h3>
                        <div style="line-height: 2rem;">
                            Базы для обновления<br/>
                            Базы для обновления<br/>
                            Базы для обновления<br/>
                            Базы для обновления<br/>
                        </div>
                    </div>
                    <div class="mb-5">
                        <h3 class="mb-3 text-black">ДОПОЛНИТЕЛЬНЫЕ УСЛУГИ</h3>
                        <div style="line-height: 2rem;">
                            Базы для обновления<br/>
                            Базы для обновления<br/>
                            Базы для обновления<br/>
                            Базы для обновления<br/>
                        </div>
                    </div>
                    <h3 class="text-secondary mb-5" style="font-size: 36px">~27 000 Р</h3>
                    <div>
                        <button class="btn btn-secondary px-4 mb-5">Оставить заявку</button>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check2">
                        <label class="custom-control-label small" for="check2">
                            Я соглашаюсь с политикой
                            конфиденциальности Я соглашаюсь с политикой
                            конфиденциальности
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
