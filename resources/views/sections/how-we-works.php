<section>
    <div class="container">
        <h1 class="text-center mb-80">Как мы работаем</h1>
        <div class="row text-center no-gutters">
            <div class="col-12 col-md-4 algorithm-item">
                <h3 class="algorithm-item__header px-md-40">Экспресс-обследование</h3>
                <div class="algorithm-item__text">
                    Детальное обследование бизнес- процессов<br/>
                    Анализ текущих задач<br/>
                    Информация о сроках<br/>
                    Выбор подходящего продукта
                </div>
            </div>
            <div class="col-12 col-md-4 algorithm-item">
                <h3 class="algorithm-item__header px-md-40">Моделирование</h3>
                <div class="algorithm-item__text">
                    Определение целей продукта<br/>
                    Составление функциональных требований<br/>
                    Демонстрация типового функцианала пользователям
                </div>
            </div>
            <div class="col-12 col-md-4 algorithm-item">
                <h3 class="algorithm-item__header px-md-40">Проектирование</h3>
                <div class="algorithm-item__text">
                    Составление технического задания<br/>
                    Обучение ответственных <br class="d-none d-md-inline"/>пользователей<br/>
                    Определяются конкретные требования к&nbsp;результатам внедрения<br/>
                </div>
            </div>
        </div>
    </div>
</section>