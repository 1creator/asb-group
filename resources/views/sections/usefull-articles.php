<section class="bg-circle bg-circle--2">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="text-center mb-80">Полезные статьи</h1>
            </div>
        </div>
        <div class="row">
            <div class="article-preview-card col-12 col-md-6 mb-100 pr-md-60 d-flex flex-column">
                <h3 class="mb-20">Нужны ли услуги программиста 1С? Разумеется, нет! Кому нужны отлаженные
                    бизнес-процессы</h3>
                <div class="d-flex mt-auto">
                    <span class="article-preview-card__date">Январь 20, 2019</span>
                    <a href="/blog/1" class='btn btn-link ml-auto'>
                        Читать дальше
                        <i class="text-primary icon-sm icon-caret-right"></i>
                    </a>
                </div>
            </div>
            <div class="article-preview-card col-12 col-md-6 mb-100 pl-md-60 d-flex flex-column">
                <h3 class="mb-20">Нужны ли услуги программиста 1С? </h3>
                <div class="d-flex mt-auto">
                    <span class="article-preview-card__date">Январь 20, 2019</span>
                    <a href="/blog/1" class='btn btn-link ml-auto'>
                        Читать дальше
                        <i class="text-primary icon-sm icon-caret-right"></i>
                    </a>
                </div>
            </div>
            <div class="article-preview-card col-12 col-md-6 mb-100 pr-md-60 d-flex flex-column">
                <h3 class="mb-20">Нужны ли услуги программиста 1С? Разумеется</h3>
                <div class="d-flex mt-auto">
                    <span class="article-preview-card__date">Январь 20, 2019</span>
                    <a href="/blog/1" class='btn btn-link ml-auto'>
                        Читать дальше
                        <i class="text-primary icon-sm icon-caret-right"></i>
                    </a>
                </div>
            </div>
            <div class="article-preview-card col-12 col-md-6 mb-100 pl-md-60 d-flex flex-column">
                <h3 class="mb-20">Нужны ли услуги программиста 1С? Разумеется, нет!</h3>
                <div class="d-flex mt-auto">
                    <span class="article-preview-card__date">Январь 20, 2019</span>
                    <a href="/blog/1" class='btn btn-link ml-auto'>
                        Читать дальше
                        <i class="text-primary icon-sm icon-caret-right"></i>
                    </a>
                </div>
            </div>
            <div class="article-preview-card col-12 col-md-6 mb-100 mb-md-0 pr-md-60 d-flex flex-column">
                <h3 class="mb-20">Кому нужны отлаженные
                    бизнес-процессы</h3>
                <div class="d-flex mt-auto">
                    <span class="article-preview-card__date">Январь 20, 2019</span>
                    <a href="/blog/1" class='btn btn-link ml-auto'>
                        Читать дальше
                        <i class="text-primary icon-sm icon-caret-right"></i>
                    </a>
                </div>
            </div>
            <div class="article-preview-card col-12 col-md-6 mb-100 mb-md-0 pl-md-60 d-flex flex-column">
                <h3 class="mb-20">Нужны ли услуги программиста 1С? Разумеется, нет! Кому нужны отлаженные
                    бизнес-процессы</h3>
                <div class="d-flex mt-auto">
                    <span class="article-preview-card__date">Январь 20, 2019</span>
                    <a href="/blog/1" class='btn btn-link ml-auto'>
                        Читать дальше
                        <i class="text-primary icon-sm icon-caret-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>