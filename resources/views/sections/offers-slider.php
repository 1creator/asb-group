<div class="glide offers-slider" data-glide-perview="1">
    <div class="glide__track" data-glide-el="track">
        <ul class="glide__slides">
            <li class="glide__slide">
                <div class="offer text-white">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-7 text-center text-md-left offer__text">
                                <h1 class="mb-40">
                                    Низкая производительность труда в компании?
                                </h1>
                                <div class="mb-40">
                                    Возможности, которые вы получаете:<br/>
                                    Использование широкого функционала 1С
                                </div>
                                <a href="/offers" class="btn btn-secondary border-0 offer__btn">Акции</a>
                            </div>
                            <div class="offer__bg-image">
                                <img src="/images/man.png">
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="glide__slide">
                <div class="offer text-white">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-7 text-center text-md-left offer__text">
                                <h1 class="mb-40">
                                    Низкая производительность труда в компании?
                                </h1>
                                <div class="mb-40">
                                    Возможности, которые вы получаете:<br/>
                                    Использование широкого функционала 1С
                                </div>
                                <a href="/offers" class="btn btn-secondary border-0 offer__btn">Акции</a>
                            </div>
                            <div class="offer__bg-image">
                                <img src="/images/man.png">
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="glide__slide">
                <div class="offer text-white">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-7 text-center text-md-left offer__text">
                                <h1 class="mb-40">
                                    Низкая производительность труда в компании?
                                </h1>
                                <div class="mb-40">
                                    Возможности, которые вы получаете:<br/>
                                    Использование широкого функционала 1С
                                </div>
                                <a href="/offers" class="btn btn-secondary border-0 offer__btn">Акции</a>
                            </div>
                            <div class="offer__bg-image">
                                <img src="/images/man.png">
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <div class="container offers-slider__controls d-none d-md-block">
            <div class="col" data-glide-el="controls">
                <button data-glide-dir="<" class="btn btn-sm btn-primary btn-circle border-white">
                    <i class="icon-triangle-left"></i>
                </button>
                <span class="offers-slider__indices mx-2 text-white">1/5</span>
                <button data-glide-dir=">" class="btn btn-sm btn-primary btn-circle border-white">
                    <i class="icon-triangle-right"></i>
                </button>
            </div>
        </div>
    </div>
</div>