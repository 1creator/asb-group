<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 mb-80">
                <h1 class="text-center">Внедрение 1С - Порядок в вашем бизнесе</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4 service-item mb-md-0">
                <div class="service-item__img-wrap">
                    <img class="mb-3" src="/images/shield.svg">
                </div>
                <h3>Не удобно работать в 1С?</h3>
                <div class="text-gray">Использование широкого функционала 1С</div>
            </div>
            <div class="col-12 col-md-4 service-item mb-md-0">
                <div class="service-item__img-wrap">
                    <img class="mb-3" src="/images/shield.svg">
                </div>
                <h3>Неквалифицированные сотрудники?</h3>
                <div class="text-gray">
                    Доработка функцианала<br/>
                    Обучение персонала
                </div>
            </div>
            <div class="col-12 col-md-4 service-item mb-md-0">
                <div class="service-item__img-wrap">
                    <img class="mb-3" src="/images/shield.svg">
                </div>
                <h3>Не удобно управлять?</h3>
                <div class="text-gray">
                    Получить индивидуально настроенную систему<br/>под текущие бизнес-процессы предприятия
                </div>
            </div>
        </div>
    </div>
</section>