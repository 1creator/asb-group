<section>
    <div class="container">
        <h2 class="text-center mb-80">Отраслевые решения</h2>
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <div class="row">
                    <a href="/industries/1" class="col-6 col-md-3 service-item">
                        <div class="service-item__img-wrap">
                            <img class="mb-3" src="/images/agriculture.svg">
                        </div>
                        <div>Сельское и лесное хозяйство</div>
                    </a>
                    <a href="/industries/1" class="col-6 col-md-3 service-item">
                        <div class="service-item__img-wrap">
                            <img class="mb-3" src="/images/manufacturing.svg">
                        </div>
                        <div>Производство, ТЭК</div>
                    </a>
                    <a href="/industries/1" class="col-6 col-md-3 service-item">
                        <div class="service-item__img-wrap">
                            <img class="mb-3" src="/images/building.svg">
                        </div>
                        <div>Строительство, девелопмент, ЖКХ</div>
                    </a>
                    <a href="/industries/1" class="col-6 col-md-3 service-item">
                        <div class="service-item__img-wrap">
                            <img class="mb-3" src="/images/trade.svg">
                        </div>
                        <div>Торговля, склад, логистика, транспорт</div>
                    </a>
                    <a href="/industries/1" class="col-6 col-md-3 service-item">
                        <div class="service-item__img-wrap">
                            <img class="mb-3" src="/images/management.svg">
                        </div>
                        <div>Государственное и муниципальное управление</div>
                    </a>
                    <a href="/industries/1" class="col-6 col-md-3 service-item">
                        <div class="service-item__img-wrap">
                            <img class="mb-3" src="/images/prof-service.svg">
                        </div>
                        <div>Профессиональные услуги</div>
                    </a>
                    <a href="/industries/1" class="col-6 col-md-3 service-item">
                        <div class="service-item__img-wrap">
                            <img class="mb-3" src="/images/medicine.svg">
                        </div>
                        <div>Здравоохранение и медицина</div>
                    </a>
                    <a href="/industries/1" class="col-6 col-md-3 service-item">
                        <div class="service-item__img-wrap">
                            <img class="mb-3" src="/images/hotels.svg">
                        </div>
                        <div>Общественное и плановое питание, гостиничный бизнес</div>
                    </a>
                    <a href="/industries/1" class="col-6 col-md-3 service-item">
                        <div class="service-item__img-wrap">
                            <img class="mb-3" src="/images/education.svg">
                        </div>
                        <div>Образование, культура</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>