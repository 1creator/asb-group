<section class="bg-circle bg-circle--1">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="text-center mb-80">Последние проекты</h1>
            </div>
        </div>
        <div class="row">
            <div class="project-preview-card col-12 col-md-6 mb-80">
                <img class="w-100 mb-20" src="/images/project-card-header.png">
                <h3 class="mb-40">ОАО "Воентелеком"ОАО "Воентелеком"ОАО "Воентелеком"</h3>
                <div class="d-flex">
                    <span class="project-preview-card__date">Январь 20, 2019</span>
                    <a href="/projects/1" class='btn btn-link ml-auto'>
                        Читать дальше
                        <i class="text-primary icon-sm icon-caret-right"></i>
                    </a>
                </div>
            </div>
            <div class="project-preview-card col-12 col-md-6 mb-80">
                <img class="w-100 mb-20" src="/images/project-card-header.png">
                <h3 class="mb-40">ОАО "Воентелеком"ОАО "Воентелеком"ОАО "Воентелеком"</h3>
                <div class="d-flex">
                    <span class="project-preview-card__date">Январь 20, 2019</span>
                    <a href="/projects/1" class='btn btn-link ml-auto'>
                        Читать дальше
                        <i class="text-primary icon-sm icon-caret-right"></i>
                    </a>
                </div>
            </div>
            <div class="project-preview-card col-12 col-md-6 mb-80">
                <img class="w-100 mb-20" src="/images/project-card-header.png">
                <h3 class="mb-40">ОАО "Воентелеком"ОАО "Воентелеком"ОАО "Воентелеком"</h3>
                <div class="d-flex">
                    <span class="project-preview-card__date">Январь 20, 2019</span>
                    <a href="/projects/1" class='btn btn-link ml-auto'>
                        Читать дальше
                        <i class="text-primary icon-sm icon-caret-right"></i>
                    </a>
                </div>
            </div>
            <div class="project-preview-card col-12 col-md-6 mb-80">
                <img class="w-100 mb-20" src="/images/project-card-header.png">
                <h3 class="mb-40">ОАО "Воентелеком"ОАО "Воентелеком"ОАО "Воентелеком"</h3>
                <div class="d-flex">
                    <span class="project-preview-card__date">Январь 20, 2019</span>
                    <a href="/projects/1" class='btn btn-link ml-auto'>
                        Читать дальше
                        <i class="text-primary icon-sm icon-caret-right"></i>
                    </a>
                </div>
            </div>
            <div class="project-preview-card col-12 col-md-6 mb-40 mb-md-0">
                <img class="w-100 mb-20" src="/images/project-card-header.png">
                <h3 class="mb-40">ОАО "Воентелеком"ОАО "Воентелеком"ОАО "Воентелеком"</h3>
                <div class="d-flex">
                    <span class="project-preview-card__date">Январь 20, 2019</span>
                    <a href="/projects/1" class='btn btn-link ml-auto'>
                        Читать дальше
                        <i class="text-primary icon-sm icon-caret-right"></i>
                    </a>
                </div>
            </div>
            <div class="project-preview-card col-12 col-md-6 mb-40 mb-md-0">
                <img class="w-100 mb-20" src="/images/project-card-header.png">
                <h3 class="mb-40">ОАО "Воентелеком"ОАО "Воентелеком"ОАО "Воентелеком"</h3>
                <div class="d-flex">
                    <span class="project-preview-card__date">Январь 20, 2019</span>
                    <a href="/projects/1" class='btn btn-link ml-auto'>
                        Читать дальше
                        <i class="text-primary icon-sm icon-caret-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>