<section class="bg-circle bg-circle--3">
    <div class="container text-center">
        <h2 class="mb-20">Остались вопросы?</h2>
        <div class="row mb-40">
            <div class="col-12 col-sm-6 offset-sm-3">
                Если у вас остались вопросы, вы можете оставить номер телефона и мы с вами свяжемся
            </div>
        </div>
        <form action="/" method="post">
            <div class="row mb-20">
                <div class="col-12 col-sm-8 offset-sm-2">
                    <div class="form-row mb-40">
                        <div class="col">
                            <input type="text" class="w-100 form-control" placeholder="Имя" required>
                        </div>
                        <div class="col">
                            <input type="tel" class="w-100 form-control" placeholder="Номер телефона" required>
                        </div>
                    </div>
                    <div class="custom-control custom-checkbox mb-40">
                        <input type="checkbox" class="custom-control-input" id="check2" required>
                        <label class="custom-control-label small" for="check2">
                            Я соглашаюсь с политикой конфиденциальности
                        </label>
                    </div>
                    <button class="btn btn-secondary mb-3">Заказать звонок</button>
                </div>
            </div>
        </form>
    </div>
</section>