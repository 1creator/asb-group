<section>
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="text-center mb-100">Как мы облегчаем жизнь?</h1>
            </div>
        </div>
        <div class="row flex-column-reverse flex-md-row">
            <div class="col-12 col-md-7">
                <ul class="list-big-markers">
                    <li class="mb-4">
                        <h3>Отслеживание заработка</h3>
                        <div>Team expenses - 84 500 (30%)</div>
                    </li>
                    <li class="mb-4">
                        <h3>Отслеживание трат</h3>
                        <div>Team expenses - 84 500 (30%)</div>
                    </li>
                    <li class="mb-4">
                        <h3>Сравнение показателей</h3>
                        <div>Exchanges 42 500 (20%)</div>
                    </li>
                    <li class="mb-4">
                        <h3>Управление бизнесом</h3>
                        <div>Exchanges 42 500 (20%)</div>
                    </li>
                    <li class="mb-4">
                        <h3>Эффективность работы сотрудников</h3>
                        <div>Team expenses - 84 500 (30%)</div>
                    </li>
                    <li class="mb-4">
                        <h3>Эффективность и производительность труда</h3>
                        <div>Exchanges 42 500 (20%)</div>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-5 align-self-center">
                <div class="doughnut ml-md-n80">
                    <canvas class="chart chart--doughnut doughnut__chart"></canvas>
                    <div class="doughnut__text font-weight-bold">
                        <div>252 000</div>
                        <div>пользуются</div>
                        <div>услугами</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>