<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>О компании</title>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <script src="/js/app.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg py-md-40">
    <div class="container flex-wrap">
        <div class="row no-gutters w-100 align-items-center">
            <a class="navbar-brand" href="/">
                <img src="/images/logo.svg">
                <small class="text-light-gray d-none d-md-inline">Системы автоматизации бизнеса</small>
            </a>
            <ul class="list-unstyled navbar-nav ml-auto align-self-center d-none d-md-flex">
                <li class="nav-item">
                    <a class="nav-link nav__phone" href="tel:89998402030">8 999 840 20 30</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link pr-0 nav__phone" href="tel:89998073016">8 999 807 30 16</a>
                </li>
            </ul>
            <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="icon-hamburger text-secondary"></i>
            </button>
        </div>
        <div class="row no-gutters w-100 collapse navbar-collapse align-items-end" id="navbarSupportedContent">
            <div class="col-md-6 col-12 no-gutters">
                <ul class="col-12 list-unstyled navbar-nav mr-auto nav__top-menu my-md-3">
                    <li class="nav-item">
                        <a class="nav-link pl-0" href="/about">О компании</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/blog">Полезное</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/contacts">Контакты</a>
                    </li>
                </ul>
                <ul class="col-12 list-unstyled navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link pl-0" href="/service">Услуги</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/products">Программы 1С</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/industries">Отраслевые решения</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/projects">Проекты</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-12 no-gutters">
                <div class="nav__form form-inline my-2 my-md-3 no-gutters">
                    <div class="input-group col-12 col-md-5 col-lg-4 ml-auto mb-3 mb-md-0">
                        <input class="form-control border-right-0 h-100 border-primary" type="search">
                        <span class="input-group-append">
                        <div class="input-group-text bg-transparent border-primary"><i
                                    class="text-primary icon-search"></i></div>
                    </span>
                    </div>
                    <button class="btn btn-primary col-12 col-md-5 col-lg-4 ml-md-3" data-toggle="modal"
                            data-target="#call-modal">
                        Заказать звонок
                    </button>
                </div>
                <ul class="col-12 list-unstyled navbar-nav ml-auto flex-row">
                    <li class="nav-item ml-auto px-2">
                        <a class="nav-link" href="mailto:info.spb@rarus.ru">info.spb@rarus.ru</a>
                    </li>
                    <li class="nav-item px-2">
                        <a class="nav-link text-secondary" href="/#"><i class="icon-phone"></i></a>
                    </li>
                    <li class="nav-item px-2">
                        <a class="nav-link text-secondary" href="/#"><i class="icon-telegram"></i></a>
                    </li>
                    <li class="nav-item px-2">
                        <a class="nav-link text-secondary pr-0" href="/#"><i class="icon-whatsapp"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<div class="container">
    <div class="page-tree">
        <a class="page-tree__item" href="#">Главная</a>
        <a class="page-tree__item" href="#">О компании</a>
    </div>
</div>
<section style="background-image: url('/images/company-people.png')" class="bg-img bg-img-blue">
</section>
<section>
    <div class="container">
        <div class="row text-center">
            <div class="col-12 col-md-10 offset-md-1 mb-80">
                <h1 class="mb-20">ASB GROUP</h1>
                <h3 class="mb-20">Команда опытных специалистов в области 1С установки и внедрения</h3>
                <small class="mx-md-20">Многолетний опыт работы с бизнесом самого разного масштаба — от
                    индивидуальных<br class="d-none d-lg-inline"/>
                    предпринимателей до больших предприятий с разветвлённой структурой.
                </small>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4 service-item mb-md-0">
                <div class="service-item__img-wrap">
                    <img class="mb-3" src="/images/responsibility.svg">
                </div>
                <h3>Мы ответственны</h3>
                <div class="text-gray">Использование широкого функционала 1С</div>
            </div>
            <div class="col-12 col-md-4 service-item mb-md-0">
                <div class="service-item__img-wrap">
                    <img class="mb-3" src="/images/fast-work.svg">
                </div>
                <h3>Мы быстро работаем</h3>
                <div class="text-gray">
                    Доработка функцианала<br/>
                    Обучение персонала
                </div>
            </div>
            <div class="col-12 col-md-4 service-item mb-md-0">
                <div class="service-item__img-wrap">
                    <img class="mb-3" src="/images/experience.svg">
                </div>
                <h3>10 лет опыт</h3>
                <div class="text-gray">
                    Получить индивидуально настроенную систему под&nbsp;текущие&nbsp;бизнес-процессы предприятия
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row text-center">
            <div class="col-12 col-md-8 offset-md-2 mb-80">
                <h1 class="mb-20">Наши специалисты</h1>
            </div>
        </div>
        <div class="nav row mb-80" role="tablist" onclick="updateSpecialistCard();">
            <div class="col-12 col-lg-6 d-flex align-items-center specialist-preview-card mb-80"
                 data-toggle="tab" data-target="#specialist-1" data-role="tab">
                <div class="specialist-card__avatar-wrap">
                    <img src="/images/avatar.png" class="specialist-card__avatar">
                </div>
                <div>
                    <h3 class="mb-20 specialist-card__name">Васильев Денис</h3>
                    <div class="mb-20 text-gray">Программист</div>
                    <div class="mb-20">
                        The monkey-rope is found in all whalers; but it was only in the Pequod that
                        the
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 d-flex align-items-center specialist-preview-card mb-80"
                 data-toggle="tab" data-target="#specialist-2" data-role="tab">
                <div class="specialist-card__avatar-wrap">
                    <img src="/images/avatar.png" class="specialist-card__avatar">
                </div>
                <div>
                    <h3 class="mb-20 specialist-card__name">Васильев Денис</h3>
                    <div class="mb-20 text-gray">Программист</div>
                    <div class="mb-20">
                        The monkey-rope is found in all whalers; but it was only in the Pequod that
                        the
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 d-flex align-items-center specialist-preview-card mb-80"
                 data-toggle="tab" data-target="#specialist-3" data-role="tab">
                <div class="specialist-card__avatar-wrap">
                    <img src="/images/avatar.png" class="specialist-card__avatar">
                </div>
                <div>
                    <h3 class="mb-20 specialist-card__name">Васильев Денис</h3>
                    <div class="mb-20 text-gray">Программист</div>
                    <div class="mb-20">
                        The monkey-rope is found in all whalers; but it was only in the Pequod that
                        the
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 d-flex align-items-center specialist-preview-card mb-80"
                 data-toggle="tab" data-target="#specialist-4" data-role="tab">
                <div class="specialist-card__avatar-wrap">
                    <img src="/images/avatar.png" class="specialist-card__avatar">
                </div>
                <div>
                    <h3 class="mb-20 specialist-card__name">Васильев Денис</h3>
                    <div class="mb-20 text-gray">Программист</div>
                    <div class="mb-20">
                        The monkey-rope is found in all whalers; but it was only in the Pequod that
                        the
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 d-flex align-items-center specialist-preview-card mb-80 mb-lg-0"
                 data-toggle="tab" data-target="#specialist-5" data-role="tab">
                <div class="specialist-card__avatar-wrap">
                    <img src="/images/avatar.png" class="specialist-card__avatar">
                </div>
                <div>
                    <h3 class="mb-20 specialist-card__name">Васильев Денис</h3>
                    <div class="mb-20 text-gray">Программист</div>
                    <div class="mb-20">
                        The monkey-rope is found in all whalers; but it was only in the Pequod that
                        the
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 d-flex align-items-center specialist-preview-card mb-80 mb-lg-0"
                 data-toggle="tab" data-target="#specialist-6" data-role="tab">
                <div class="specialist-card__avatar-wrap">
                    <img src="/images/avatar.png" class="specialist-card__avatar">
                </div>
                <div>
                    <h3 class="mb-20 specialist-card__name">Васильев Денис</h3>
                    <div class="mb-20 text-gray">Программист</div>
                    <div class="mb-20">
                        The monkey-rope is found in all whalers; but it was only in the Pequod that
                        the
                    </div>
                </div>
            </div>
        </div>
        <a name="specialist-full-info"></a>
        <div class="tab-content">
            <div class="tab-pane fade" id="specialist-1" role="tabpanel">
                <div class="card card-shadow specialist-card">
                    <div class="card-body">
                        <div class="row no-gutters">
                            <div class="col-12 col-lg-8">
                                <div class="glide" data-glide-perview="2">
                                    <div class="glide__track" data-glide-el="track">
                                        <ul class="glide__slides">
                                            <li class="glide__slide">
                                                <img src="/images/certificate.png">
                                            </li>
                                            <li class="glide__slide">
                                                <img src="/images/certificate.png">
                                            </li>
                                        </ul>
                                        <div data-glide-el="controls" class="text-center">
                                            <button data-glide-dir="<"
                                                    class="btn btn-sm btn-primary btn-circle mr-2">
                                                <i class="icon-triangle-left"></i>
                                            </button>
                                            <button data-glide-dir=">"
                                                    class="btn btn-sm btn-primary btn-circle ">
                                                <i class="icon-triangle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4 text-lg-right d-flex flex-column">
                                <h3 class="mb-20 mt-80">Васильев Денис</h3>
                                <div class="mb-20"><strong>Опыт работы:</strong> 10 лет</div>
                                <div class="mb-20"><strong>Навыки:</strong>
                                    Проходят серьезное обучение<br/>
                                    Высококвалифицированны<br/>
                                    Особый подход
                                </div>
                                <button class="btn btn-primary ml-md-auto mt-auto">Получить консультацию</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="specialist-2" role="tabpanel">
                <div class="card card-shadow specialist-card">
                    <div class="card-body">
                        <div class="row no-gutters">
                            <div class="col-12 col-lg-8">
                                <div class="glide" data-glide-perview="2">
                                    <div class="glide__track" data-glide-el="track">
                                        <ul class="glide__slides">
                                            <li class="glide__slide">
                                                <img src="/images/certificate.png">
                                            </li>
                                            <li class="glide__slide">
                                                <img src="/images/certificate.png">
                                            </li>
                                        </ul>
                                        <div data-glide-el="controls" class="text-center">
                                            <button data-glide-dir="<"
                                                    class="btn btn-sm btn-primary btn-circle mr-2">
                                                <i class="icon-triangle-left"></i>
                                            </button>
                                            <button data-glide-dir=">"
                                                    class="btn btn-sm btn-primary btn-circle ">
                                                <i class="icon-triangle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4 text-lg-right d-flex flex-column">
                                <h3 class="mb-20 mt-80">Андреев Андрей</h3>
                                <div class="mb-20"><strong>Опыт работы:</strong> 10 лет</div>
                                <div class="mb-20"><strong>Навыки:</strong>
                                    Проходят серьезное обучение<br/>
                                    Высококвалифицированны<br/>
                                    Особый подход
                                </div>
                                <button class="btn btn-primary ml-md-auto mt-auto">Получить консультацию</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="specialist-3" role="tabpanel">
                <div class="card card-shadow specialist-card">
                    <div class="card-body">
                        <div class="row no-gutters">
                            <div class="col-12 col-lg-8">
                                <div class="glide" data-glide-perview="2">
                                    <div class="glide__track" data-glide-el="track">
                                        <ul class="glide__slides">
                                            <li class="glide__slide">
                                                <img src="/images/certificate.png">
                                            </li>
                                            <li class="glide__slide">
                                                <img src="/images/certificate.png">
                                            </li>
                                        </ul>
                                        <div data-glide-el="controls" class="text-center">
                                            <button data-glide-dir="<"
                                                    class="btn btn-sm btn-primary btn-circle mr-2">
                                                <i class="icon-triangle-left"></i>
                                            </button>
                                            <button data-glide-dir=">"
                                                    class="btn btn-sm btn-primary btn-circle ">
                                                <i class="icon-triangle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4 text-lg-right d-flex flex-column">
                                <h3 class="mb-20 mt-80">Иванов Иван</h3>
                                <div class="mb-20"><strong>Опыт работы:</strong> 10 лет</div>
                                <div class="mb-20"><strong>Навыки:</strong>
                                    Проходят серьезное обучение<br/>
                                    Высококвалифицированны<br/>
                                    Особый подход
                                </div>
                                <button class="btn btn-primary ml-md-auto mt-auto">Получить консультацию</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="specialist-4" role="tabpanel">
                <div class="card card-shadow specialist-card">
                    <div class="card-body">
                        <div class="row no-gutters">
                            <div class="col-12 col-lg-8">
                                <div class="glide" data-glide-perview="2">
                                    <div class="glide__track" data-glide-el="track">
                                        <ul class="glide__slides">
                                            <li class="glide__slide">
                                                <img src="/images/certificate.png">
                                            </li>
                                            <li class="glide__slide">
                                                <img src="/images/certificate.png">
                                            </li>
                                        </ul>
                                        <div data-glide-el="controls" class="text-center">
                                            <button data-glide-dir="<"
                                                    class="btn btn-sm btn-primary btn-circle mr-2">
                                                <i class="icon-triangle-left"></i>
                                            </button>
                                            <button data-glide-dir=">"
                                                    class="btn btn-sm btn-primary btn-circle ">
                                                <i class="icon-triangle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4 text-lg-right d-flex flex-column">
                                <h3 class="mb-20 mt-80">Алексеев Иван</h3>
                                <div class="mb-20"><strong>Опыт работы:</strong> 10 лет</div>
                                <div class="mb-20"><strong>Навыки:</strong>
                                    Проходят серьезное обучение<br/>
                                    Высококвалифицированны<br/>
                                    Особый подход
                                </div>
                                <button class="btn btn-primary ml-md-auto mt-auto">Получить консультацию</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="specialist-5" role="tabpanel">
                <div class="card card-shadow specialist-card">
                    <div class="card-body">
                        <div class="row no-gutters">
                            <div class="col-12 col-lg-8">
                                <div class="glide" data-glide-perview="2">
                                    <div class="glide__track" data-glide-el="track">
                                        <ul class="glide__slides">
                                            <li class="glide__slide">
                                                <img src="/images/certificate.png">
                                            </li>
                                            <li class="glide__slide">
                                                <img src="/images/certificate.png">
                                            </li>
                                        </ul>
                                        <div data-glide-el="controls" class="text-center">
                                            <button data-glide-dir="<"
                                                    class="btn btn-sm btn-primary btn-circle mr-2">
                                                <i class="icon-triangle-left"></i>
                                            </button>
                                            <button data-glide-dir=">"
                                                    class="btn btn-sm btn-primary btn-circle ">
                                                <i class="icon-triangle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4 text-lg-right d-flex flex-column">
                                <h3 class="mb-20 mt-80">Морозов Антон</h3>
                                <div class="mb-20"><strong>Опыт работы:</strong> 10 лет</div>
                                <div class="mb-20"><strong>Навыки:</strong>
                                    Проходят серьезное обучение<br/>
                                    Высококвалифицированны<br/>
                                    Особый подход
                                </div>
                                <button class="btn btn-primary ml-md-auto mt-auto">Получить консультацию</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="specialist-6" role="tabpanel">
                <div class="card card-shadow specialist-card">
                    <div class="card-body">
                        <div class="row no-gutters">
                            <div class="col-12 col-lg-8">
                                <div class="glide" data-glide-perview="2">
                                    <div class="glide__track" data-glide-el="track">
                                        <ul class="glide__slides">
                                            <li class="glide__slide">
                                                <img src="/images/certificate.png">
                                            </li>
                                            <li class="glide__slide">
                                                <img src="/images/certificate.png">
                                            </li>
                                        </ul>
                                        <div data-glide-el="controls" class="text-center">
                                            <button data-glide-dir="<"
                                                    class="btn btn-sm btn-primary btn-circle mr-2">
                                                <i class="icon-triangle-left"></i>
                                            </button>
                                            <button data-glide-dir=">"
                                                    class="btn btn-sm btn-primary btn-circle ">
                                                <i class="icon-triangle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4 text-lg-right d-flex flex-column">
                                <h3 class="mb-20 mt-80">Васильев Денис</h3>
                                <div class="mb-20"><strong>Опыт работы:</strong> 10 лет</div>
                                <div class="mb-20"><strong>Навыки:</strong>
                                    Проходят серьезное обучение<br/>
                                    Высококвалифицированны<br/>
                                    Особый подход
                                </div>
                                <button class="btn btn-primary ml-md-auto mt-auto">Получить консультацию</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row mb-80">
            <div class="col text-center">
                <h1>Что о нас говорят</h1>
                <a class="h4 text-secondary" href="/reviews">Читать все отзывы</a>
            </div>
        </div>
        <div class="glide mb-80" data-glide-perview="2" data-glide-gap="40">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides mb-40" style="overflow: visible">
                    <li class="glide__slide">
                        <div class="card card-shadow review-card">
                            <div class="card-header review-card__video-wrap">
                                <button data-glide-dir=">" class="btn btn-sm btn-light btn-circle">
                                    <i class="icon-triangle-right"></i>
                                </button>
                            </div>
                            <div class="card-body">
                                <div class="d-flex align-items- mb-4">
                                    <div class="review-card__avatar mr-3">
                                        <img src="/images/avatar.png">
                                    </div>
                                    <div>
                                        <h3>Вадим</h3>
                                        <div class="text-gray">Компания 1С</div>
                                    </div>
                                </div>
                                <div class="review-card__text">
                                    Многолетний опыт работы с бизнесом самого разного масштаба — от индивидуальных
                                    предпринимателей до больших предприятий с разветвлённой структурой.
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <div data-glide-el="controls" class="text-center">
                    <button data-glide-dir="<" class="btn btn-sm btn-primary btn-circle mr-2">
                        <i class="icon-triangle-left"></i>
                    </button>
                    <button data-glide-dir=">" class="btn btn-sm btn-primary btn-circle">
                        <i class="icon-triangle-right"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="glide" data-glide-perview="2" data-glide-gap="40">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides mb-40" style="overflow: visible">
                    <li class="glide__slide">
                        <div class="card card-shadow review-card mb-40">
                            <div class="card-body">
                                <div class="d-flex align-items- mb-4">
                                    <div class="review-card__avatar mr-3">
                                        <img src="/images/avatar.png">
                                    </div>
                                    <div>
                                        <h3>Вадим</h3>
                                        <div class="text-gray">Компания 1С</div>
                                    </div>
                                </div>
                                <div class="review-card__text">
                                    Многолетний опыт работы с бизнесом самого разного масштаба — от индивидуальных
                                    предпринимателей до больших предприятий с разветвлённой структурой.
                                </div>
                                <button class="btn btn-link text-decoration-none mt-2 mb-2"
                                        data-toggle="collapse" data-target="#review-card-1-read-more">
                                    Читать больше
                                    <i class="text-primary icon-sm icon-arrow-down"></i>
                                </button>
                                <div class="collapse" id="review-card-1-read-more">
                                    <div class="d-flex mb-2">
                                        ................<br/>
                                        ................<br/>
                                        ................<br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-shadow review-card">
                            <div class="card-body">
                                <div class="d-flex align-items- mb-4">
                                    <div class="review-card__avatar mr-3">
                                        <img src="/images/avatar.png">
                                    </div>
                                    <div>
                                        <h3>Вадим</h3>
                                        <div class="text-gray">Компания 1С</div>
                                    </div>
                                </div>
                                <div class="review-card__text">
                                    Многолетний опыт работы с бизнесом самого разного масштаба — от индивидуальных
                                    предпринимателей до больших предприятий с разветвлённой структурой.
                                </div>
                                <button class="btn btn-link text-decoration-none mt-2 mb-2"
                                        data-toggle="collapse" data-target="#review-card-2-read-more">
                                    Читать больше
                                    <i class="text-primary icon-sm icon-arrow-down"></i>
                                </button>
                                <div class="collapse" id="review-card-2-read-more">
                                    <div class="d-flex mb-2">
                                        ................<br/>
                                        ................<br/>
                                        ................<br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="glide__slide">
                        <div class="card card-shadow review-card mb-40">
                            <div class="card-body">
                                <div class="d-flex align-items- mb-4">
                                    <div class="review-card__avatar mr-3">
                                        <img src="/images/avatar.png">
                                    </div>
                                    <div>
                                        <h3>Вадим</h3>
                                        <div class="text-gray">Компания 1С</div>
                                    </div>
                                </div>
                                <div class="review-card__text">
                                    Многолетний опыт работы с бизнесом самого разного масштаба — от индивидуальных
                                    предпринимателей до больших предприятий с разветвлённой структурой.
                                </div>
                                <button class="btn btn-link text-decoration-none mt-2 mb-2"
                                        data-toggle="collapse" data-target="#review-card-3-read-more">
                                    Читать больше
                                    <i class="text-primary icon-sm icon-arrow-down"></i>
                                </button>
                                <div class="collapse" id="review-card-3-read-more">
                                    <div class="d-flex mb-2">
                                        ................<br/>
                                        ................<br/>
                                        ................<br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-shadow review-card">
                            <div class="card-body">
                                <div class="d-flex align-items- mb-4">
                                    <div class="review-card__avatar mr-3">
                                        <img src="/images/avatar.png">
                                    </div>
                                    <div>
                                        <h3>Вадим</h3>
                                        <div class="text-gray">Компания 1С</div>
                                    </div>
                                </div>
                                <div class="review-card__text">
                                    Многолетний опыт работы с бизнесом самого разного масштаба — от индивидуальных
                                    предпринимателей до больших предприятий с разветвлённой структурой.
                                </div>
                                <button class="btn btn-link text-decoration-none mt-2 mb-2"
                                        data-toggle="collapse" data-target="#review-card-4-read-more">
                                    Читать больше
                                    <i class="text-primary icon-sm icon-arrow-down"></i>
                                </button>
                                <div class="collapse" id="review-card-4-read-more">
                                    <div class="d-flex mb-2">
                                        ................<br/>
                                        ................<br/>
                                        ................<br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <div data-glide-el="controls" class="text-center">
                    <button data-glide-dir="<" class="btn btn-sm btn-primary btn-circle mr-2">
                        <i class="icon-triangle-left"></i>
                    </button>
                    <button data-glide-dir=">" class="btn btn-sm btn-primary btn-circle">
                        <i class="icon-triangle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="why-we">
    <div class="container text-black">
        <div class="text-center">
            <h2 class="mb-20">Почему выбирают нас?</h2>
            <div class="mb-5">ASB GROUP - Официальный партнёр компании «1С»</div>
        </div>
        <img src="/images/map.svg" class="d-block d-md-none mb-4">
        <div class="row">
            <div class="col-12 col-md-3 no-gutters">
                <div class="row h-100 align-items-end">
                    <div class="mb-3 col-sm-6 col-md-12">
                        <h1>8 лет</h1>
                        <div>успешное существование на рынке</div>
                    </div>
                    <div class="mb-4 col-sm-6 col-md-12">
                        <h1>20</h1>
                        <div>высококвалифицированных сотрудников</div>
                    </div>
                    <div class="mb-4 col-sm-6 col-md-12">
                        <h1>10 000+</h1>
                        <div style="letter-spacing: -0.3px;">численность персонала компаний, которые мы
                            автоматизируем
                        </div>
                    </div>
                    <div class="md-4 mb-4 mb-md-0 col-sm-6 col-md-12">
                        <h1>100+</h1>
                        <div>средних и крупных проектов</div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 offset-md-1 align-self-end">
                <div class="d-none d-md-block">
                    <img src="/images/map.svg">
                </div>
                <div class="row align-items-end">
                    <div class="col-12 col-sm-6 mb-6 mb-md-0 d-flex justify-content-md-end">
                        <div>
                            <h1>8 – 10 лет</h1>
                            <div>стаж работы наших сотрудников</div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 text-right mb-6 mb-md-0">
                        <button class='btn btn-link'>
                            Читать больше
                            <i class="text-primary icon-sm icon-caret-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-circle bg-circle--3">
    <div class="container text-center">
        <h2 class="mb-20">Закажите звонок сейчас!</h2>
        <div class="row mb-40">
            <div class="col-12 col-sm-6 offset-sm-3">
                Если у вас остались вопросы, вы можете оставить номер телефона и мы с вами свяжемся
            </div>
        </div>
        <form action="/" method="post">
            <div class="row mb-20">
                <div class="col-12 col-sm-8 offset-sm-2">
                    <div class="form-row mb-40">
                        <div class="col">
                            <input type="text" class="w-100 form-control" placeholder="Имя" required>
                        </div>
                        <div class="col">
                            <input type="tel" class="w-100 form-control" placeholder="Номер телефона" required>
                        </div>
                    </div>
                    <div class="custom-control custom-checkbox mb-40">
                        <input type="checkbox" class="custom-control-input" id="check2" required>
                        <label class="custom-control-label small" for="check2">
                            Я соглашаюсь с политикой конфиденциальности
                        </label>
                    </div>
                    <button class="btn btn-secondary mb-3">Заказать звонок</button>
                </div>
            </div>
        </form>
    </div>
</section>

<div class="modal fade" id="call-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="mb-20">Закажите звонок сейчас!</h2>
                <div class="mb-20">
                    Если у вас остались вопросы, вы можете оставить номер телефона и мы с вами свяжемся
                </div>
                <form action="/" method="post">
                    <div class="mb-20">
                        <input type="text" class="w-100 form-control" placeholder="Имя" required>
                    </div>
                    <div class="mb-20">
                        <input type="tel" class="w-100 form-control" placeholder="Номер телефона" required>
                    </div>
                    <div class="custom-control custom-checkbox mb-20">
                        <input type="checkbox" class="custom-control-input" id="call-modal-check" required>
                        <label class="custom-control-label small" for="call-modal-check">
                            Я соглашаюсь с политикой конфиденциальности
                        </label>
                    </div>
                    <button class="btn btn-primary">Заказать звонок</button>
                </form>
            </div>
        </div>
    </div>
</div>
<footer class="bg-secondary text-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-3">
                <h3 class="mb-20">О компании</h3>
                <ul class="list-unstyled">
                    <li><a class="w-100" href="/">Главная</a></li>
                    <li><a class="w-100" href="/about">О компании</a></li>
                    <li><a class="w-100" href="/price">Прайс лист</a></li>
                    <li><a class="w-100" href="/projects">Проекты</a></li>
                    <li><a class="w-100" href="/news">Новости</a></li>
                    <li><a class="w-100" href="/contacts">Контакты</a></li>
                    <li><a class="w-100" href="/offers">Акции</a></li>
                    <li><a class="w-100" href="/clients">Наши клиенты</a></li>
                    <li><a class="w-100" href="/blog">Блог</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4 col-lg-3">
                <h3 class="mb-20">Продукты</h3>
                <ul class="list-unstyled">
                    <li><a class="w-100" href="#">1С: Бухгалтерия</a></li>
                    <li><a class="w-100" href="#">1С: Зарплата и Управление Персоналом</a></li>
                    <li><a class="w-100" href="#">1С: Управление Торговлей</a></li>
                    <li><a class="w-100" href="#">1С: Управление Производственным Предприятием</a></li>
                    <li><a class="w-100" href="#">1С: Консолидация</a></li>
                    <li><a class="w-100" href="#">Мебель в 1С</a></li>
                    <li><a class="w-100" href="#">Расчет раздвижных дверей</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4 col-lg-3">
                <h3 class="mb-20">Услуги</h3>
                <ul class="list-unstyled">
                    <li><a class="w-100" href="#">Установка 1С</a></li>
                    <li><a class="w-100" href="#">Поддержка 1С</a></li>
                    <li><a class="w-100" href="#">Постановка управленческого и финансового учета</a></li>
                    <li><a class="w-100" href="#">Программирование 1С</a></li>
                    <li><a class="w-100" href="#">Оптимизация работы систем 1С</a></li>
                    <li><a class="w-100" href="#">Продажа программных продуктов 1С</a></li>
                    <li><a class="w-100" href="#">Консалтинговые услуги</a></li>
                    <li><a class="w-100" href="#">Курсы 1С:Бухгалтерия и 1С</a></li>
                    <li><a class="w-100" href="#">Предприятие</a></li>
                    <li><a class="w-100" href="#">Обучение 1С</a></li>
                    <li><a class="w-100" href="#">1С: ИТС</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 text-left text-lg-right">
                <div>
                    <a href="tel:89998073016" class="footer__phone">8 999 807 30 16</a>
                </div>
                <div class="mb-20">
                    <a href="tel:89998402030" class="footer__phone">8 999 840 20 30</a>
                </div>
                <button class="btn btn-light mb-20 text-primary" data-toggle="modal"
                        data-target="#call-modal">Заказать звонок
                </button>
                <div class="mb-20">
                    <a href="mailto:info@drgrp.ru" class="footer__email">info@drgrp.ru</a>
                </div>
                <div class="mb-20">
                    115280, г. Москва,<br/>
                    м. Электрозаводская,<br/>
                    площадь Журавлева 2,<br/>
                    строение 2, офис 329
                </div>
                <a class="text-white pr-3" href="/blog"><i class="icon-phone"></i></a>
                <a class="text-white pr-3" href="/blog"><i class="icon-telegram"></i></a>
                <a class="text-white" href="/blog"><i class="icon-whatsapp"></i></a>
            </div>
        </div>
    </div>
</footer>
</body>
</html>