<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Продукт</title>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <script src="/js/app.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg py-md-40">
    <div class="container flex-wrap">
        <div class="row no-gutters w-100 align-items-center">
            <a class="navbar-brand" href="/">
                <img src="/images/logo.svg">
                <small class="text-light-gray d-none d-md-inline">Системы автоматизации бизнеса</small>
            </a>
            <ul class="list-unstyled navbar-nav ml-auto align-self-center d-none d-md-flex">
                <li class="nav-item">
                    <a class="nav-link nav__phone" href="tel:89998402030">8 999 840 20 30</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link pr-0 nav__phone" href="tel:89998073016">8 999 807 30 16</a>
                </li>
            </ul>
            <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="icon-hamburger text-secondary"></i>
            </button>
        </div>
        <div class="row no-gutters w-100 collapse navbar-collapse align-items-end" id="navbarSupportedContent">
            <div class="col-md-6 col-12 no-gutters">
                <ul class="col-12 list-unstyled navbar-nav mr-auto nav__top-menu my-md-3">
                    <li class="nav-item">
                        <a class="nav-link pl-0" href="/about">О компании</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/blog">Полезное</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/contacts">Контакты</a>
                    </li>
                </ul>
                <ul class="col-12 list-unstyled navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link pl-0" href="/service">Услуги</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/products">Программы 1С</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/industries">Отраслевые решения</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/projects">Проекты</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-12 no-gutters">
                <div class="nav__form form-inline my-2 my-md-3 no-gutters">
                    <div class="input-group col-12 col-md-5 col-lg-4 ml-auto mb-3 mb-md-0">
                        <input class="form-control border-right-0 h-100 border-primary" type="search">
                        <span class="input-group-append">
                        <div class="input-group-text bg-transparent border-primary"><i
                                    class="text-primary icon-search"></i></div>
                    </span>
                    </div>
                    <button class="btn btn-primary col-12 col-md-5 col-lg-4 ml-md-3" data-toggle="modal"
                            data-target="#call-modal">
                        Заказать звонок
                    </button>
                </div>
                <ul class="col-12 list-unstyled navbar-nav ml-auto flex-row">
                    <li class="nav-item ml-auto px-2">
                        <a class="nav-link" href="mailto:info.spb@rarus.ru">info.spb@rarus.ru</a>
                    </li>
                    <li class="nav-item px-2">
                        <a class="nav-link text-secondary" href="/#"><i class="icon-phone"></i></a>
                    </li>
                    <li class="nav-item px-2">
                        <a class="nav-link text-secondary" href="/#"><i class="icon-telegram"></i></a>
                    </li>
                    <li class="nav-item px-2">
                        <a class="nav-link text-secondary pr-0" href="/#"><i class="icon-whatsapp"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
    <div class="container">
        <div class="page-tree">
            <a class="page-tree__item" href="#">Главная</a>
            <a class="page-tree__item" href="#">Услуги</a>
            <a class="page-tree__item" href="#">Комплексная автоматизация</a>
            <a class="page-tree__item" href="#">1C:ERP</a>
        </div>
    </div>
    <section>
        <div class="container">
            <h2 class="text-center mb-40">1С:ERP</h2>
            <div class="row align-items-end">
                <div class="col-12 col-md-8">
                    <div class="mb-40">
                        <h3 class="mb-4 text-secondary">Порядок в вашем бизнесе</h3>
                        <div>
                            Автоматизация на основе 1C:ERP, в отличие от отдельных функциональных решений, связывает
                            работу
                            всех
                            подразделений. Она позволяет построить комплексную информационную систему управления
                            предприятием,
                            повысить прозрачность бизнес-процессов и обеспечить принятие эффективных управленческих
                            решений.
                        </div>
                    </div>
                    <div class="mb-40">
                        <h3 class="mb-4 text-secondary">Для чего используют «1С:ERP»?</h3>
                        <div>
                            1С:ERP — логическое продолжение развития программного продукта «1С:Управление
                            производственным предприятием 8». 1С:ERP имеет не только гораздо более широкий функционал,
                            но и явные технологические преимущества. Учитывая то, что модернизация и внедрение системы
                            класса ERP на предприятии – сложная задача, переход с 1С:УПП на ERP может быть осуществлен
                            только с помощью команды специалистов
                        </div>
                    </div>
                    <div class="no-gutters">
                        <div class="mb-40 mb-md-0 col-md-8">
                            <h3 class="mb-4 text-secondary">Функциональные возможности</h3>
                            <ul>
                                <li>Своевременное обновление конфигураций</li>
                                <li>Своевременное обновление конфигураций, дает</li>
                                <li>Своевременное обновление конфигураций, дает получение обновление конфигураций</li>
                                <li>Своевременное обновление конфигураций, дает</li>
                                <li>Своевременное обновление конфигураций</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 text-center">
                    <div class="mb-2 mx-n40">
                        <img class="w-100" src="/images/sample-product.png">
                    </div>
                    <h3 class="mb-4">Заказать!</h3>
                    <input class="form-control mb-3" placeholder="Имя">
                    <input class="form-control mb-4" placeholder="Номер телефона">
                    <button class="btn btn-secondary">Заказать звонок</button>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="text-center mb-40">
                <h2 class="text-center mb-40">Стоимость программы 1C:ERP</h2>
                <button class="btn btn-link">
                    Перейти к прайсу
                    <i class="text-primary icon-sm icon-caret-right"></i>
                </button>
            </div>
            <div class="row price">
                <div class="col-12 col-md-3">
                    <div class="nav flex-column justify-content-sm-between flex-sm-row flex-md-column price__tabs"
                         id="v-pills-tab" role="tablist"
                         aria-orientation="vertical">
                        <a class="h3 nav-link active" data-toggle="pill" href="#variants"
                           role="tab" aria-controls="variants" aria-selected="true">Варианты</a>
                        <a class="h3 nav-link" data-toggle="pill" href="#options"
                           role="tab" aria-controls="options" aria-selected="false">Доп. опции</a>
                        <a class="h3 nav-link" data-toggle="pill" href="#your-choice"
                           role="tab" aria-controls="your-choice" aria-selected="false">Вам подойдет</a>
                    </div>
                </div>
                <div class="col-12 col-md-8 offset-md-1">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="variants" role="tabpanel"
                             aria-labelledby="v-pills-home-tab">
                            <table class="table table-borderless table-responsive table--no-left-pad mb-5 price-table">
                                <tbody>
                                <tr>
                                    <td class="" style="width: 340px">
                                        <div class="h3 font-weight-norma price-table__caption">Обновление
                                            программ 1С
                                        </div>
                                    </td>
                                    <td class="">
                                        <div class="h3 price-table__price">111 000 р.</div>
                                    </td>
                                    <td class="">
                                        <button class="btn btn-secondary">Купить</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        <div class="h3 font-weight-normal price-table__caption">Обновление
                                            программ 1С
                                        </div>
                                    </td>
                                    <td>
                                        <div class="h3 price-table__price">111 000 р.</div>
                                    </td>
                                    <td class="">
                                        <button class="btn btn-secondary">Купить</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        <div class="h3 font-weight-normal price-table__caption">Консультация по
                                            работе с
                                            программой
                                        </div>
                                    </td>
                                    <td>
                                        <div class="h3 price-table__price">111 000р.</div>
                                    </td>
                                    <td class="">
                                        <button class="btn btn-secondary">Купить</button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="options" role="tabpanel"
                             aria-labelledby="v-pills-profile-tab">
                            <table class="table table-borderless table-responsive table--no-left-pad mb-5 price-table">
                                <tbody>
                                <tr>
                                    <td class="" style="width: 340px">
                                        <div class="h3 font-weight-normal price-table__caption">Обновление
                                            программ 1С
                                        </div>
                                    </td>
                                    <td>
                                        <div class="h3 price-table__price">111 000 р.</div>
                                    </td>
                                    <td>
                                        <button class="btn btn-secondary">Купить</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        <div class="h3 font-weight-normal price-table__caption">Обновление
                                            программ 1С
                                        </div>
                                    </td>
                                    <td class="">
                                        <div class="h3 price-table__price">111 000 р.</div>
                                    </td>
                                    <td>
                                        <button class="btn btn-secondary">Купить</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        <div class="h3 font-weight-normal price-table__caption">Консультация по
                                            работе с
                                            программой
                                        </div>
                                    </td>
                                    <td>
                                        <div class="h3 price-table__price">111 000р.</div>
                                    </td>
                                    <td>
                                        <button class="btn btn-secondary">Купить</button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="your-choice" role="tabpanel"
                             aria-labelledby="v-pills-profile-tab">
                            <table class="table table-borderless table-responsive table--no-left-pad mb-5 price-table">
                                <tbody>
                                <tr>
                                    <td class="" style="width: 340px">
                                        <div class="h3 font-weight-normal price-table__caption">Обновление
                                            программ 1С
                                        </div>
                                    </td>
                                    <td>
                                        <div class="h3 price-table__price">111 000 р.</div>
                                    </td>
                                    <td>
                                        <button class="btn btn-secondary">Купить</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        <div class="h3 font-weight-normal price-table__caption">Обновление
                                            программ 1С
                                        </div>
                                    </td>
                                    <td class="">
                                        <div class="h3 price-table__price">111 000 р.</div>
                                    </td>
                                    <td>
                                        <button class="btn btn-secondary">Купить</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="">
                                        <div class="h3 font-weight-normal price-table__caption">Консультация по
                                            работе с
                                            программой
                                        </div>
                                    </td>
                                    <td>
                                        <div class="h3 price-table__price">111 000р.</div>
                                    </td>
                                    <td>
                                        <button class="btn btn-secondary">Купить</button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section>
        <div class="container">
            <div class="row mb-80">
                <div class="col text-center">
                    <h1>Комплексная автоматизация</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="card card-bg-white card-shadow text-center">
                        <div class="card-body">
                            <h5 class="card-title">Консультационное внедрение</h5>
                            <p class="card-text mb-4">
                                преимущества различных
                                проектных подходов
                                получение готовой
                                автоматизированную систему
                            </p>
                            <button class="btn btn-secondary">Выбрать</button>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card card-bg-white card-shadow text-center">
                        <div class="card-body">
                            <h5 class="card-title">Консультационное внедрение</h5>
                            <p class="card-text mb-4">
                                преимущества различных
                                проектных подходов
                                получение готовой
                                автоматизированную систему
                            </p>
                            <button class="btn btn-secondary">Выбрать</button>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card card-bg-white card-shadow text-center">
                        <div class="card-body">
                            <h5 class="card-title">Консультационное внедрение</h5>
                            <p class="card-text mb-4">
                                преимущества различных
                                проектных подходов
                                получение готовой
                                автоматизированную систему
                            </p>
                            <button class="btn btn-secondary">Выбрать</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-circle bg-circle--4">
        <div class="container text-center">
            <h2 class="mb-20">Оставьте заявку!</h2>
            <div class="row mb-40">
                <div class="col-12 col-sm-6 offset-sm-3">
                    Если у вас остались вопросы, вы можете оставить номер телефона и мы с вами свяжемся
                </div>
            </div>
            <form action="/" method="post">
                <div class="row mb-20">
                    <div class="col-12 col-sm-8 offset-sm-2">
                        <div class="form-row mb-40">
                            <div class="col">
                                <input type="text" class="w-100 form-control" placeholder="Имя" required>
                            </div>
                            <div class="col">
                                <input type="tel" class="w-100 form-control" placeholder="Номер телефона" required>
                            </div>
                        </div>
                        <div class="custom-control custom-checkbox mb-40">
                            <input type="checkbox" class="custom-control-input" id="check2" required>
                            <label class="custom-control-label small" for="check2">
                                Я соглашаюсь с политикой конфиденциальности
                            </label>
                        </div>
                        <button class="btn btn-secondary mb-3">Заказать звонок</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <section  class="bg-circle bg-circle--7">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 offset-sm-0 offset-md-1 align-self-end">
                    <div class="row">
                        <div class="col-12 questions-item mb-40">
                            <h1 class="text-secondary questions-item__index">01</h1>
                            <h3 class="questions-item__question">Не удобно работать в 1С?</h3>
                            <div class="text-muted questions-item__text pr-md-5">
                                Использование широкого функционала 1С
                            </div>
                        </div>
                        <div class="col-12 questions-item mb-40">
                            <h1 class="text-secondary questions-item__index">02</h1>
                            <h3 class="questions-item__question">Неквалифицированные сотрудники?</h3>
                            <div class="text-muted questions-item__text">
                                Доработка функционала<br/>
                                Обучение персонала
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 offset-md-1 mb-5 align-self-center">
                    <img src="/images/funny-man.svg">
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 offset-md-1">
                    <div class="questions-item mb-40">
                        <h1 class="text-secondary questions-item__index">03</h1>
                        <h3 class="questions-item__question">Не удобно управлять?</h3>
                        <div class="text-muted questions-item__text pr-md-80">
                            Получить индивидуально настроенную систему под&nbsp;текущие бизнес-процессы предприятия
                        </div>
                    </div>
                    <div class="questions-item mb-40 mb-md-0">
                        <h1 class="text-secondary questions-item__index">04</h1>
                        <h3 class="questions-item__question">Не получается установить?</h3>
                        <div class="text-muted questions-item__text">
                            Экономии времени на информации<br/>
                            Оптимизации управления<br/>
                            Хранения корпоративных документов
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 offset-md-2 d-flex flex-column justify-content-between">
                    <div class="questions-item mb-40">
                        <h1 class="text-secondary questions-item__index">05</h1>
                        <h3 class="questions-item__question">Слишком дорого?</h3>
                        <div class="text-muted questions-item__text">
                            Экономия до 50 тысяч рублей в месяц
                        </div>
                    </div>
                    <div class="questions-item">
                        <h1 class="text-secondary questions-item__index">06</h1>
                        <h3 class="questions-item__question">Не подходит типовое решение?</h3>
                        <div class="text-muted questions-item__text">
                            Индивидуальная доработка<br/>
                            Внешняя специфика
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <h1 class="mb-80 text-center">Какие проблемы решаем</h1>
            <div class="row">
                <div class="col-12 col-md-4 pr-md-40">
                    <h3 class="mb-3">Материальные</h3>
                    <div class="text-gray">
                        Зарплата и соответствующие выплаты;<br/>
                        Организация рабочего места;<br/>
                        Обучение сотрудника;<br/>
                        Вы можете планировать бюджет на год вперед с точностью до 85%
                    </div>
                </div>
                <div class="col-12 col-md-4 pr-md-40">
                    <h3 class="mb-3">Эмоциональные</h3>
                    <div class="text-gray">
                        Выяснений мелких вопросов<br/>
                        Выяснений вопросов при отсутствии сотрудника<br/>
                        Возможность отслеживать на расстоянии<br/>
                    </div>
                </div>
                <div class="col-12 col-md-4 pr-md-40">
                    <h3 class="mb-3">Материальные</h3>
                    <div class="text-gray">
                        Зарплата и соответствующие выплаты;<br/>
                        Организация рабочего места;<br/>
                        Обучение сотрудника;<br/>
                        Вы можете планировать бюджет на год вперед с точностью до 85%
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-circle bg-circle--8">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1 class="text-center mb-100">Как мы облегчаем жизнь?</h1>
                </div>
            </div>
            <div class="row flex-column-reverse flex-md-row">
                <div class="col-12 col-md-7">
                    <ul class="list-big-markers">
                        <li class="mb-4">
                            <h3>Отслеживание заработка</h3>
                            <div>Team expenses - 84 500 (30%)</div>
                        </li>
                        <li class="mb-4">
                            <h3>Отслеживание трат</h3>
                            <div>Team expenses - 84 500 (30%)</div>
                        </li>
                        <li class="mb-4">
                            <h3>Сравнение показателей</h3>
                            <div>Exchanges 42 500 (20%)</div>
                        </li>
                        <li class="mb-4">
                            <h3>Управление бизнесом</h3>
                            <div>Exchanges 42 500 (20%)</div>
                        </li>
                        <li class="mb-4">
                            <h3>Эффективность работы сотрудников</h3>
                            <div>Team expenses - 84 500 (30%)</div>
                        </li>
                        <li class="mb-4">
                            <h3>Эффективность и производительность труда</h3>
                            <div>Exchanges 42 500 (20%)</div>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-5 align-self-center">
                    <div class="doughnut ml-md-n80">
                        <canvas class="chart chart--doughnut doughnut__chart"></canvas>
                        <div class="doughnut__text font-weight-bold">
                            <div>252 000</div>
                            <div>пользуются</div>
                            <div>услугами</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-circle bg-circle--3">
        <div class="container text-center">
            <h2 class="mb-20">Остались вопросы?</h2>
            <div class="row mb-40">
                <div class="col-12 col-sm-6 offset-sm-3">
                    Если у вас остались вопросы, вы можете оставить номер телефона и мы с вами свяжемся
                </div>
            </div>
            <form action="/" method="post">
                <div class="row mb-20">
                    <div class="col-12 col-sm-8 offset-sm-2">
                        <div class="form-row mb-40">
                            <div class="col">
                                <input type="text" class="w-100 form-control" placeholder="Имя" required>
                            </div>
                            <div class="col">
                                <input type="tel" class="w-100 form-control" placeholder="Номер телефона" required>
                            </div>
                        </div>
                        <div class="custom-control custom-checkbox mb-40">
                            <input type="checkbox" class="custom-control-input" id="check2" required>
                            <label class="custom-control-label small" for="check2">
                                Я соглашаюсь с политикой конфиденциальности
                            </label>
                        </div>
                        <button class="btn btn-secondary mb-3">Заказать звонок</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <section class="why-we">
        <div class="container text-black">
            <div class="text-center">
                <h2 class="mb-20">Почему выбирают нас?</h2>
                <div class="mb-5">ASB GROUP - Официальный партнёр компании «1С»</div>
            </div>
            <img src="/images/map.svg" class="d-block d-md-none mb-4">
            <div class="row">
                <div class="col-12 col-md-3 no-gutters">
                    <div class="row h-100 align-items-end">
                        <div class="mb-3 col-sm-6 col-md-12">
                            <h1>8 лет</h1>
                            <div>успешное существование на рынке</div>
                        </div>
                        <div class="mb-4 col-sm-6 col-md-12">
                            <h1>20</h1>
                            <div>высококвалифицированных сотрудников</div>
                        </div>
                        <div class="mb-4 col-sm-6 col-md-12">
                            <h1>10 000+</h1>
                            <div style="letter-spacing: -0.3px;">численность персонала компаний, которые мы автоматизируем</div>
                        </div>
                        <div class="md-4 mb-4 mb-md-0 col-sm-6 col-md-12">
                            <h1>100+</h1>
                            <div>средних и крупных проектов</div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-8 offset-md-1 align-self-end">
                    <div class="d-none d-md-block">
                        <img src="/images/map.svg">
                    </div>
                    <div class="row align-items-end">
                        <div class="col-12 col-sm-6 mb-6 mb-md-0 d-flex justify-content-md-end">
                            <div>
                                <h1>8 – 10 лет</h1>
                                <div>стаж работы наших сотрудников</div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 text-right mb-6 mb-md-0">
                            <button class='btn btn-link'>
                                Читать больше
                                <i class="text-primary icon-sm icon-caret-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <h1 class="text-center mb-80">Как мы работаем</h1>
            <div class="row text-center no-gutters">
                <div class="col-12 col-md-4 algorithm-item">
                    <h3 class="algorithm-item__header px-md-40">Экспресс-обследование</h3>
                    <div class="algorithm-item__text">
                        Детальное обследование бизнес- процессов<br/>
                        Анализ текущих задач<br/>
                        Информация о сроках<br/>
                        Выбор подходящего продукта
                    </div>
                </div>
                <div class="col-12 col-md-4 algorithm-item">
                    <h3 class="algorithm-item__header px-md-40">Моделирование</h3>
                    <div class="algorithm-item__text">
                        Определение целей продукта<br/>
                        Составление функциональных требований<br/>
                        Демонстрация типового функцианала пользователям
                    </div>
                </div>
                <div class="col-12 col-md-4 algorithm-item">
                    <h3 class="algorithm-item__header px-md-40">Проектирование</h3>
                    <div class="algorithm-item__text">
                        Составление технического задания<br/>
                        Обучение ответственных <br class="d-none d-md-inline"/>пользователей<br/>
                        Определяются конкретные требования к&nbsp;результатам внедрения<br/>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="best-offers-wrap">
        <div class="container py-80 best-offers">
            <div class="row text-white mb-80">
                <div class="col-12 col-md-4 best-offers__text">
                    <h2 class="mb-4">Наши лучшие предложения</h2>
                    <div>
                        Возможности, которые вы получаете:<br/>
                        Использование широкого функционала 1С
                    </div>
                </div>
                <div class="col-md-8 best-offers__image-wrap">
                    <img src="/images/computer.svg">
                </div>
            </div>
            <div class="glide" data-glide-gap="30">
                <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides mb-80">
                        <li class="glide__slide">
                            <div class="card py-20 card-bg-additional text-center">
                                <div class="card-body">
                                    <div class="mb-20">
                                        <img src="/images/icons/people-money.svg">
                                    </div>
                                    <h5 class="card-title" style="height:80px">1С:ERP<br/>Управление предприятием</h5>
                                    <p class="card-text mb-40">1С:ERP Управление предприятием 1С:ERP Управление
                                        предприятием</p>
                                    <button class="btn btn-secondary">Читать подробнее</button>
                                </div>
                            </div>
                        </li>
                        <li class="glide__slide">
                            <div class="card py-20 card-bg-additional text-center">
                                <div class="card-body">
                                    <div class="mb-20">
                                        <img src="/images/icons/people-money.svg">
                                    </div>
                                    <h5 class="card-title" style="height:80px">ИТС</h5>
                                    <p class="card-text mb-40">1С:ERP Управление предприятием 1С:ERP Управление
                                        предприятием</p>
                                    <button class="btn btn-secondary">Читать подробнее</button>
                                </div>
                            </div>
                        </li>
                        <li class="glide__slide">
                            <div class="card py-20 card-bg-additional text-center">
                                <div class="card-body">
                                    <div class="mb-20">
                                        <img src="/images/icons/people-money.svg">
                                    </div>
                                    <h5 class="card-title" style="height:80px">1С: Обучение</h5>
                                    <p class="card-text mb-40">1С:ERP Управление предприятием 1С:ERP Управление
                                        предприятием</p>
                                    <button class="btn btn-secondary">Читать подробнее</button>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div data-glide-el="controls" class="text-center">
                        <button data-glide-dir="<" class="btn btn-sm border-white btn-secondary btn-circle mr-2">
                            <i class="icon-triangle-left"></i>
                        </button>
                        <button data-glide-dir=">" class="btn btn-sm border-white btn-secondary btn-circle">
                            <i class="icon-triangle-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-circle bg-circle--1">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1 class="text-center mb-80">Последние проекты</h1>
                </div>
            </div>
            <div class="row">
                <div class="project-preview-card col-12 col-md-6 mb-80">
                    <img class="w-100 mb-20" src="/images/project-card-header.png">
                    <h3 class="mb-40">ОАО "Воентелеком"ОАО "Воентелеком"ОАО "Воентелеком"</h3>
                    <div class="d-flex">
                        <span class="project-preview-card__date">Январь 20, 2019</span>
                        <a href="/projects/1" class='btn btn-link ml-auto'>
                            Читать дальше
                            <i class="text-primary icon-sm icon-caret-right"></i>
                        </a>
                    </div>
                </div>
                <div class="project-preview-card col-12 col-md-6 mb-80">
                    <img class="w-100 mb-20" src="/images/project-card-header.png">
                    <h3 class="mb-40">ОАО "Воентелеком"ОАО "Воентелеком"ОАО "Воентелеком"</h3>
                    <div class="d-flex">
                        <span class="project-preview-card__date">Январь 20, 2019</span>
                        <a href="/projects/1" class='btn btn-link ml-auto'>
                            Читать дальше
                            <i class="text-primary icon-sm icon-caret-right"></i>
                        </a>
                    </div>
                </div>
                <div class="project-preview-card col-12 col-md-6 mb-80">
                    <img class="w-100 mb-20" src="/images/project-card-header.png">
                    <h3 class="mb-40">ОАО "Воентелеком"ОАО "Воентелеком"ОАО "Воентелеком"</h3>
                    <div class="d-flex">
                        <span class="project-preview-card__date">Январь 20, 2019</span>
                        <a href="/projects/1" class='btn btn-link ml-auto'>
                            Читать дальше
                            <i class="text-primary icon-sm icon-caret-right"></i>
                        </a>
                    </div>
                </div>
                <div class="project-preview-card col-12 col-md-6 mb-80">
                    <img class="w-100 mb-20" src="/images/project-card-header.png">
                    <h3 class="mb-40">ОАО "Воентелеком"ОАО "Воентелеком"ОАО "Воентелеком"</h3>
                    <div class="d-flex">
                        <span class="project-preview-card__date">Январь 20, 2019</span>
                        <a href="/projects/1" class='btn btn-link ml-auto'>
                            Читать дальше
                            <i class="text-primary icon-sm icon-caret-right"></i>
                        </a>
                    </div>
                </div>
                <div class="project-preview-card col-12 col-md-6 mb-40 mb-md-0">
                    <img class="w-100 mb-20" src="/images/project-card-header.png">
                    <h3 class="mb-40">ОАО "Воентелеком"ОАО "Воентелеком"ОАО "Воентелеком"</h3>
                    <div class="d-flex">
                        <span class="project-preview-card__date">Январь 20, 2019</span>
                        <a href="/projects/1" class='btn btn-link ml-auto'>
                            Читать дальше
                            <i class="text-primary icon-sm icon-caret-right"></i>
                        </a>
                    </div>
                </div>
                <div class="project-preview-card col-12 col-md-6 mb-40 mb-md-0">
                    <img class="w-100 mb-20" src="/images/project-card-header.png">
                    <h3 class="mb-40">ОАО "Воентелеком"ОАО "Воентелеком"ОАО "Воентелеком"</h3>
                    <div class="d-flex">
                        <span class="project-preview-card__date">Январь 20, 2019</span>
                        <a href="/projects/1" class='btn btn-link ml-auto'>
                            Читать дальше
                            <i class="text-primary icon-sm icon-caret-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-circle bg-circle--3">
        <div class="container text-center">
            <h2 class="mb-20">Остались вопросы?</h2>
            <div class="row mb-40">
                <div class="col-12 col-sm-6 offset-sm-3">
                    Если у вас остались вопросы, вы можете оставить номер телефона и мы с вами свяжемся
                </div>
            </div>
            <form action="/" method="post">
                <div class="row mb-20">
                    <div class="col-12 col-sm-8 offset-sm-2">
                        <div class="form-row mb-40">
                            <div class="col">
                                <input type="text" class="w-100 form-control" placeholder="Имя" required>
                            </div>
                            <div class="col">
                                <input type="tel" class="w-100 form-control" placeholder="Номер телефона" required>
                            </div>
                        </div>
                        <div class="custom-control custom-checkbox mb-40">
                            <input type="checkbox" class="custom-control-input" id="check2" required>
                            <label class="custom-control-label small" for="check2">
                                Я соглашаюсь с политикой конфиденциальности
                            </label>
                        </div>
                        <button class="btn btn-secondary mb-3">Заказать звонок</button>
                    </div>
                </div>
            </form>
        </div>
    </section>

<div class="modal fade" id="call-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="mb-20">Закажите звонок сейчас!</h2>
                <div class="mb-20">
                    Если у вас остались вопросы, вы можете оставить номер телефона и мы с вами свяжемся
                </div>
                <form action="/" method="post">
                    <div class="mb-20">
                        <input type="text" class="w-100 form-control" placeholder="Имя" required>
                    </div>
                    <div class="mb-20">
                        <input type="tel" class="w-100 form-control" placeholder="Номер телефона" required>
                    </div>
                    <div class="custom-control custom-checkbox mb-20">
                        <input type="checkbox" class="custom-control-input" id="call-modal-check" required>
                        <label class="custom-control-label small" for="call-modal-check">
                            Я соглашаюсь с политикой конфиденциальности
                        </label>
                    </div>
                    <button class="btn btn-primary">Заказать звонок</button>
                </form>
            </div>
        </div>
    </div>
</div>
<footer class="bg-secondary text-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-3">
                <h3 class="mb-20">О компании</h3>
                <ul class="list-unstyled">
                    <li><a class="w-100" href="/">Главная</a></li>
                    <li><a class="w-100" href="/about">О компании</a></li>
                    <li><a class="w-100" href="/price">Прайс лист</a></li>
                    <li><a class="w-100" href="/projects">Проекты</a></li>
                    <li><a class="w-100" href="/news">Новости</a></li>
                    <li><a class="w-100" href="/contacts">Контакты</a></li>
                    <li><a class="w-100" href="/offers">Акции</a></li>
                    <li><a class="w-100" href="/clients">Наши клиенты</a></li>
                    <li><a class="w-100" href="/blog">Блог</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4 col-lg-3">
                <h3 class="mb-20">Продукты</h3>
                <ul class="list-unstyled">
                    <li><a class="w-100" href="#">1С: Бухгалтерия</a></li>
                    <li><a class="w-100" href="#">1С: Зарплата и Управление Персоналом</a></li>
                    <li><a class="w-100" href="#">1С: Управление Торговлей</a></li>
                    <li><a class="w-100" href="#">1С: Управление Производственным Предприятием</a></li>
                    <li><a class="w-100" href="#">1С: Консолидация</a></li>
                    <li><a class="w-100" href="#">Мебель в 1С</a></li>
                    <li><a class="w-100" href="#">Расчет раздвижных дверей</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4 col-lg-3">
                <h3 class="mb-20">Услуги</h3>
                <ul class="list-unstyled">
                    <li><a class="w-100" href="#">Установка 1С</a></li>
                    <li><a class="w-100" href="#">Поддержка 1С</a></li>
                    <li><a class="w-100" href="#">Постановка управленческого и финансового учета</a></li>
                    <li><a class="w-100" href="#">Программирование 1С</a></li>
                    <li><a class="w-100" href="#">Оптимизация работы систем 1С</a></li>
                    <li><a class="w-100" href="#">Продажа программных продуктов 1С</a></li>
                    <li><a class="w-100" href="#">Консалтинговые услуги</a></li>
                    <li><a class="w-100" href="#">Курсы 1С:Бухгалтерия и 1С</a></li>
                    <li><a class="w-100" href="#">Предприятие</a></li>
                    <li><a class="w-100" href="#">Обучение 1С</a></li>
                    <li><a class="w-100" href="#">1С: ИТС</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 text-left text-lg-right">
                <div>
                    <a href="tel:89998073016" class="footer__phone">8 999 807 30 16</a>
                </div>
                <div class="mb-20">
                    <a href="tel:89998402030" class="footer__phone">8 999 840 20 30</a>
                </div>
                <button class="btn btn-light mb-20 text-primary" data-toggle="modal"
                        data-target="#call-modal">Заказать звонок
                </button>
                <div class="mb-20">
                    <a href="mailto:info@drgrp.ru" class="footer__email">info@drgrp.ru</a>
                </div>
                <div class="mb-20">
                    115280, г. Москва,<br/>
                    м. Электрозаводская,<br/>
                    площадь Журавлева 2,<br/>
                    строение 2, офис 329
                </div>
                <a class="text-white pr-3" href="/blog"><i class="icon-phone"></i></a>
                <a class="text-white pr-3" href="/blog"><i class="icon-telegram"></i></a>
                <a class="text-white" href="/blog"><i class="icon-whatsapp"></i></a>
            </div>
        </div>
    </div>
</footer>
</body>
</html>