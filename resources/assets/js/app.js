require('bootstrap');
require('chart.js');
import Glide from '@glidejs/glide'

/**
 * custom doughnut chart setup
 */
Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.doughnut);
let helpers = Chart.helpers;
Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
    updateElement: function (arc, index, reset) {
        var _this = this;
        var chart = _this.chart,
            chartArea = chart.chartArea,
            opts = chart.options,
            animationOpts = opts.animation,
            arcOpts = opts.elements.arc,
            centerX = (chartArea.left + chartArea.right) / 2,
            centerY = (chartArea.top + chartArea.bottom) / 2,
            startAngle = opts.rotation, // non reset case handled later
            endAngle = opts.rotation, // non reset case handled later
            dataset = _this.getDataset(),
            circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
            innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
            outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
            custom = arc.custom || {},
            valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

        helpers.extend(arc, {
            // Utility
            _datasetIndex: _this.index,
            _index: index,

            // Desired view properties
            _model: {
                x: centerX + chart.offsetX,
                y: centerY + chart.offsetY,
                startAngle: startAngle,
                endAngle: endAngle,
                circumference: circumference,
                outerRadius: outerRadius,
                innerRadius: innerRadius,
                label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
            },

            draw: function () {
                var ctx = this._chart.ctx,
                    vm = this._view,
                    sA = vm.startAngle,
                    eA = vm.endAngle,
                    opts = this._chart.config.options;

                var labelPos = this.tooltipPosition();
                var segmentLabel = vm.circumference / opts.circumference * 100;

                ctx.beginPath();

                ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
                ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);

                ctx.closePath();
                ctx.strokeStyle = vm.borderColor;
                ctx.lineWidth = vm.borderWidth;

                ctx.fillStyle = vm.backgroundColor;

                ctx.fill();
                ctx.lineJoin = 'bevel';

                if (vm.borderWidth) {
                    ctx.stroke();
                }

                if (vm.circumference > 0.15) { // Trying to hide label when it doesn't fit in segment
                    ctx.beginPath();
                    ctx.font = helpers.fontString(opts.defaultFontSize, opts.defaultFontStyle, opts.defaultFontFamily);
                    ctx.fillStyle = "#fff";
                    ctx.textBaseline = "top";
                    ctx.textAlign = "center";

                    // Round percentage in a way that it always adds up to 100%
                    ctx.fillText(segmentLabel.toFixed(0) + "%", labelPos.x, labelPos.y);
                }
            }
        });

        var model = arc._model;
        model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
        model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
        model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
        model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

        // Set correct angles if not resetting
        if (!reset || !animationOpts.animateRotate) {
            if (index === 0) {
                model.startAngle = opts.rotation;
            } else {
                model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
            }

            model.endAngle = model.startAngle + model.circumference;
        }

        arc.pivot();
    }
});

/**
 function for init chart bar
 @param element canvas
 */
function initDoughnut(element) {
    let ctx = element.getContext('2d');
    let chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'doughnutLabels',
        data: {
            datasets: [{
                data: [10, 20, 30, 40, 50, 60],
                backgroundColor: ['#589AFD', '#0064FB', '#3023AE', '#589AFD', '#0064FB', '#3023AE'],
                hoverBackgroundColor: ['#274c84', '#083477', '#140e47', '#274c84', '#083477', '#140e47'],
            }],
            labels: [
                '10',
                '20',
                '30',
                '40',
                '50',
                '60',
            ]
        },
        options: {
            aspectRatio: 1,
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            }
        }
    });
}

/**
 function for init chart bar
 @param element canvas
 */
function initBar(element) {
    let ctx = element.getContext("2d");

    let gradient1 = ctx.createLinearGradient(0, 0, 0, 500);
    gradient1.addColorStop(0, '#589AFD');
    gradient1.addColorStop(1, '#0064FB');

    let gradient2 = ctx.createLinearGradient(0, 0, 0, 500);
    gradient2.addColorStop(0, '#91CAFF');
    gradient2.addColorStop(1, '#589AFD');

    let data = {
        labels: ["2014", "2015", "2016", "2017", "2018"],
        datasets: [{
            label: "?",
            backgroundColor: gradient1,
            data: [170, 300, 260, 390, 660]
        }, {
            label: "?",
            backgroundColor: gradient2,
            data: [135, 250, 230, 450, 610]
        }]
    };

    let myBarChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            barValueSpacing: 20,
            scales: {
                yAxes: [{
                    ticks: {
                        userCallback: function (item) {
                            return item + " $";
                        },
                    }
                }]
            }
        }
    });

}

/**
 function for init sliders
 @param element DOM Element slider
 */
function initGlide(element) {
    let options = {};
    let perView = element.getAttribute("data-glide-perview");
    let gap = element.getAttribute("data-glide-gap");
    let slidesCount = element.querySelectorAll('.glide__slide').length;
    options.gap = gap ? gap : 10;
    if (perView) {
        options.perView = perView;
        options.breakpoints = {
            768: {
                perView: 1
            },
        }
    } else {
        options.breakpoints = {
            768: {
                perView: 1
            },
            992: {
                perView: 2
            },
            3000: {
                perView: 3
            }
        }
    }
    let glide = new Glide(element, {
        type: 'carousel',
        startAt: 0,
        ...options
    }).mount();

    let indicesSpan = element.querySelector('.offers-slider__indices');
    if (indicesSpan) {
        indicesSpan.innerHTML = `1/${slidesCount}`;
        glide.on('run', function () {
            indicesSpan.innerHTML = `${glide.index + 1}/${slidesCount}`;
        });
    }
}

/**
 function for init custom input range
 @param input DOM Element input[type=range]
 */
function initCustomRange(input) {
    let coef = 14 / input.offsetWidth * 0.5;
    input.oninput = function () {
        let value = (input.value - input.min) / (input.max - input.min);
        value -= (value >= 0.5 ? value : -(1 - value)) * coef;
        input.style.backgroundImage = [
            `-webkit-gradient(linear, left top, right top, color-stop(${value}, #3172FD), color-stop(${value}, #eaeaea))`
        ].join('');
    };
    input.dispatchEvent(new Event('input'));
}

/**
 init all elements on page
 */
document.addEventListener('DOMContentLoaded', function () {
        let inputs = document.querySelectorAll('input[type=range]');
        for (let i = 0; i < inputs.length; i++) {
            initCustomRange(inputs[i]);
        }

        let doughnuts = document.querySelectorAll('.chart--doughnut');
        for (let i = 0; i < doughnuts.length; i++) {
            initDoughnut(doughnuts[i]);
        }

        let bars = document.querySelectorAll('.chart--bar');
        for (let i = 0; i < bars.length; i++) {
            initBar(bars[i]);
        }

        let glides = document.querySelectorAll('.glide');
        for (let i = 0; i < glides.length; i++) {
            initGlide(glides[i]);
        }

        let forms = document.querySelectorAll('form');
        for (let i = 0; i < forms.length; i++) {
            forms[i].addEventListener('invalid', function (e) {
                this.classList.toggle('form-invalid', true);
            }, true);
        }

    }
);

/**
 used for right glider initialization after fade in
 example on page about
 */
window.updateSpecialistCard = function () {
    document.querySelector('[name=specialist-full-info]')
        .scrollIntoView({block: "start", behavior: "smooth"});
    setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
    }, 300);
};
